import { keywords } from '../parser/keywords.js';
import { getDecoratorInfo } from './decoratorUtils.js';

/**
 * @param {any[]} args 
 * @returns {void}
 */
export function testCase(...args) {

  return function (targetProto, childKey) {
    const decInfo = getDecoratorInfo(childKey);
    if (decInfo.isClass) return;

    const target = targetProto[decInfo.key];
    const isFunction = target instanceof Function;
    if (isFunction == false) return;

    const testCases = target[keywords.cases] || (target[keywords.cases] = []);

    testCases.push([...args]);
  }

}