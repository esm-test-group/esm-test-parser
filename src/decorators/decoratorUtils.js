export const getDecoratorInfo = key => {
  // babel
  if (key instanceof Object) {
    return {
      isClass: key.kind === 'class',
      key: key.name
    };
  }

  // typescript
  if (key == undefined) {
    return { isClass: true };
  };

  return { isClass: false, key };
}