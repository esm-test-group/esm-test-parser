import { keywords } from '../parser/keywords.js';
import { isOnlyExpressionSupported } from '../parser/utils/testOnlyUtils.js';
import { getDecoratorInfo } from './decoratorUtils.js';

/**
 * @param {number | string | void} expression 
 * @returns {void}
 */
export function testOnly() {
  const expression = arguments.length === 0 ? 1 : arguments[0];

  return function (targetProto, childKey) {
    const decInfo = getDecoratorInfo(childKey);
    const exprSupported = isOnlyExpressionSupported(expression);

    if (decInfo.isClass && exprSupported === false) {
      throw new Error(createErrorMessage(expression, targetProto.name));
    }

    if (decInfo.isClass) {
      targetProto[keywords.only] = true;
      return;
    }

    const target = targetProto[decInfo.key];
    const isFunction = target instanceof Function;

    if (isFunction === false) {
      throw new Error(createErrorMessage(expression, targetProto.constructor.name));
    }

    if (exprSupported === false) {
      throw new Error(createErrorMessage(expression, targetProto.constructor.name));
    }

    target[keywords.only] = expression === undefined ? 1 : expression;
  }
}

function createErrorMessage(expression, targetName) {
  throw new Error(
    `testOnly('${expression}') expression on '${targetName}' not supported`
  );
}