import { keywords } from '../parser/keywords.js';
import { getDecoratorInfo } from './decoratorUtils.js';

/**
 * @param {string} title 
 * @returns {void}
 */
export function testTitle(title) {

  return function (targetProto, childKey) {
    const decInfo = getDecoratorInfo(childKey);

    if (decInfo.isClass) {
      targetProto[keywords.title] = title;
      return;
    }

    const target = targetProto[decInfo.key];
    const isFunction = target instanceof Function;
    if (isFunction == false) return;

    target[keywords.title] = title;
  }

}