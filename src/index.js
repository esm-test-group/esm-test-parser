export * from './decorators/all.js';
export * from './extractTestsFromModule.js';
export * from './options/testParserFlags.js';
export * from './options/testParserOptions.js';
export * from './parser/models/testCollection.js';
export * from './parser/models/testType.js';

import { keywords } from './parser/keywords.js';

export const only = keywords.only;

export const test = {
  only: keywords.only,
  title: keywords.title,
  cases: keywords.cases
}