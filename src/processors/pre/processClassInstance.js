import { Test, TestType } from '../../parser/models/all.js';
import { ClassInstanceField } from '../../parser/models/classInstanceField.js';
import { isClassInstance } from '../../parser/utils/classUtils.js';
import { PreProcessContext } from '../models/preProcessContext.js';
import { getChildGroupName, getClassInstanceChildren } from './preProcessUtils.js';

/**
 * 
 * @param {ClassInstanceField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processClassInstance(field, index, context) {
  const { name, value } = field;

  if (isClassInstance(value)) {

    context.tests.push(
      new Test(
        field.groupName,
        name,
        value,
        new TestType(TestType.CLASS_INST),
        // test cases
        [],
        // only expression
        ""
      )
    );

    // process existing class instance
    const testProps = getClassInstanceChildren(
      getChildGroupName(field.groupName, name),
      value,
      context.options
    );

    context.tests.push.apply(
      context.tests,
      testProps
    );

    return true;
  }

  return false;
}
