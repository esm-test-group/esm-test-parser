import { keywords } from '../../parser/keywords.js';
import { Test, TestType } from '../../parser/models/all.js';
import { isClassDefinition } from "../../parser/utils/classUtils.js";
import { hasOnlyDecorator } from '../../parser/utils/testOnlyUtils.js';
import { hasTitleDecorator } from '../../parser/utils/testTitleUtils.js';
import { PreProcessContext } from '../models/preProcessContext.js';
import { getChildGroupName, getClassInstanceChildren } from './preProcessUtils.js';

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processClassDef(field, index, context) {
  const { name, value } = field;

  // class definition flags
  if (isClassDefinition(value)) {
    const isClassDefWithOnly = hasOnlyDecorator(field.value);
    const isClassDefWithTitle = hasTitleDecorator(field.value);
    const isDefaultExport = field.isDefaultExport;

    let type = TestType.CLASS_DEF;
    type |= isDefaultExport && TestType.EXPORT_DEFAULT;
    type |= isClassDefWithOnly && TestType.CLASS_DEF_WITH_ONLY;
    type |= isClassDefWithTitle && TestType.CLASS_DEF_WITH_TITLE;

    const nameOrTitle = isClassDefWithTitle
      ? value[keywords.title]
      : name;

    // avoid default entries
    if (isDefaultExport === false) {
      context.tests.push(
        new Test(
          field.groupName,
          nameOrTitle,
          value,
          new TestType(type),
          // test cases
          [],
          // only expression
          ""
        )
      );
    }

    // generate the child group name
    let childGroupName = isDefaultExport && isClassDefWithTitle === false
      ? field.groupName
      : getChildGroupName(field.groupName, nameOrTitle);

    // process new class instance
    const classInstance = new value();
    const testProps = getClassInstanceChildren(
      childGroupName,
      classInstance,
      context.options
    );

    context.tests.push.apply(
      context.tests,
      testProps
    );

    return true;
  }

  return false;
}