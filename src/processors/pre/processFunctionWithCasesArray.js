import { ObjectField, Test, TestType } from '../../parser/models/all.js';
import { getFunctionTestCases } from '../../parser/utils/testCaseUtils.js';
import { PreProcessContext } from '../models/preProcessContext.js';

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processFunctionWithCasesArray(field, index, context) {
  let { name, value } = field;

  if (field.isFunctionWithCases) {
    const testCases = getFunctionTestCases(field.value);

    context.tests.push(
      new Test(
        field.groupName,
        name,
        value[value.length - 1],
        new TestType(TestType.FUNCTION_WITH_CASES),
        testCases,
        // only expression
        ""
      )
    );

    return true;
  }

  return false;
}