import { ObjectField, Test, TestType } from "../../parser/models/all.js";
import { getTitle, hasTitle } from "../../parser/utils/testTitleUtils.js";
import { PreProcessContext } from "../models/preProcessContext.js";
import { getChildGroupName, processChildObjectFields } from "./preProcessUtils.js";

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processObject(field, index, context) {
  let { name, value } = field;

  if (value instanceof Object) {
    const options = context.options;
    const isObjectWithTitle = hasTitle(value, options);

    const nameOrTitle = isObjectWithTitle
      ? getTitle(value, options)
      : name;

    const objectTestProp = new Test(
      field.groupName,
      nameOrTitle,
      value,
      new TestType(TestType.OBJECT),
      // test cases
      [],
      // only expression
      ""
    );

    context.tests.push(objectTestProp);

    // process children
    const testProps = processChildObjectFields(
      getChildGroupName(objectTestProp.groupName, nameOrTitle),
      value,
      options
    );

    context.tests.push.apply(
      context.tests,
      testProps
    );

    return true;
  }

  return false;
}