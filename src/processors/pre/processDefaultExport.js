import { isClassDefinition } from '../../parser/utils/classUtils.js';
import { ObjectField } from '../../parser/models/objectField.js';
import { PreProcessContext } from '../models/preProcessContext.js';
import { processChildObjectFields } from './preProcessUtils.js';
import { processClassDef } from './processClassDef.js';

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processDefaultExport(field, index, context) {
  const isDefaultExport = field.isDefaultExport;

  if (isDefaultExport) {

    const { name, value } = field;

    if (isClassDefinition(value)) {
      return processClassDef(
        new ObjectField(field.groupName, name, value),
        index,
        context
      );
    }

    // process children
    const testProps = processChildObjectFields(
      field.groupName,
      value,
      context.options
    );

    context.tests.push.apply(
      context.tests,
      testProps
    );

    return true;
  }

  return false;
}