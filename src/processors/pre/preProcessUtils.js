import { TestParserOptions } from "../../options/testParserOptions.js";
import { TestCollection } from "../../parser/models/testCollection.js";
import { parserClassInstFields } from "../../parser/parseClassInstFields.js";
import { parseObjectFields } from "../../parser/parseObjectFields.js";
import { executePreProcessing } from "../executePreProcessing.js";

/**
 * 
 * @param {string} groupName
 * @param {name} name
 * @returns {string}
 */
export const getChildGroupName = (groupName, name) => `${groupName}_${name}`;

/**
 * 
 * @param {string} childGroupName 
 * @param {object} value
 * @param {TestParserOptions} options
 * @returns {TestCollection}
 */
export function processChildObjectFields(childGroupName, value, options) {
  const childFields = parseObjectFields(childGroupName, value);

  return executePreProcessing(childFields, options);
}

/**
 * 
 * @param {string} childGroupName 
 * @param {object} classInstance 
 * @param {TestParserOptions} options 
 * @returns 
 */
export function getClassInstanceChildren(childGroupName, classInstance, options) {
  defineClassContextGetter(childGroupName, classInstance, options);

  const childFields = parserClassInstFields(childGroupName, classInstance)

  return executePreProcessing(childFields, options);
}

/**
 * 
 * @param {string} groupName 
 * @param {object} thisArg 
 * @param {TestParserOptions} options 
 */
export function defineClassContextGetter(groupName, thisArg, options) {
  Object.defineProperty(
    thisArg,
    options.classContextPropertyName,
    {
      configurable: true,
      get: function () {
        return options.testContextLookup(groupName)
      },
    }
  )
}