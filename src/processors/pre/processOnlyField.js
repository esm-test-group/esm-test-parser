import { ObjectField, Test, TestType } from '../../parser/models/all.js';
import { isOnlyExpressionSupported, isOnlyField } from "../../parser/utils/testOnlyUtils.js";
import { PreProcessContext } from '../models/preProcessContext.js';

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processOnlyField(field, index, context) {
  let { name, value } = field;

  const options = context.options;
  const isOnly = isOnlyField(name, options) && isOnlyExpressionSupported(value);
  if (isOnly) {
    context.tests.push(
      new Test(
        field.groupName,
        name,
        value,
        new TestType(TestType.OBJECT_WITH_ONLY),
        // test cases
        [],
        // only expression
        value
      )
    );

    return true;
  }

  return false;
}