import { ObjectField, Test, TestType } from "../../parser/models/all.js";
import { isModule } from "../../parser/utils/moduleUtils.js";
import { getTitle, hasTitle } from "../../parser/utils/testTitleUtils.js";
import { PreProcessContext } from "../models/preProcessContext.js";
import { getChildGroupName, processChildObjectFields } from "./preProcessUtils.js";

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processModule(field, index, context) {
  const { name, value } = field;

  if (isModule(value)) {
    const options = context.options;
    const type = TestType.MODULE;
    const isModuleWithTitle = hasTitle(value, options);

    const nameOrTitle = isModuleWithTitle
      ? getTitle(value, options)
      : name;

    const moduleTestGroup = new Test(
      field.groupName,
      nameOrTitle,
      value,
      new TestType(type),
      // test cases
      [],
      // only expression
      ""
    );

    context.tests.push(moduleTestGroup);

    // process children
    const testProps = processChildObjectFields(
      getChildGroupName(moduleTestGroup.groupName, nameOrTitle),
      value,
      options
    );

    context.tests.push.apply(
      context.tests,
      testProps
    );

    return true;
  }

  return false;
}