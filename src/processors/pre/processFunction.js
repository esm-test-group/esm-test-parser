import { TestParserOptions } from '../../options/testParserOptions.js';
import { ObjectField, Test, TestType } from '../../parser/models/all.js';
import {
  getPureFunctionCases,
  hasPureFunctionCases
} from '../../parser/utils/testCaseUtils.js';
import { hasOnly } from '../../parser/utils/testOnlyUtils.js';
import { getTitle, hasTitle } from '../../parser/utils/testTitleUtils.js';
import { PreProcessContext } from '../models/preProcessContext.js';

/**
 * 
 * @param {ObjectField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processFunction(field, index, context) {
  let { name, value } = field;

  if (field.isFunction) {
    const options = context.options;
    const isFunctionBuiltIn = context.isBuiltInFunction(name);
    const isPureFunctionWithOnly = hasOnly(value, options);
    const isPureFunctionWithTitle = hasTitle(value, options);
    const isPureFunctionWithCases = hasPureFunctionCases(value, options);

    let type = TestType.FUNCTION
    type |= isFunctionBuiltIn && TestType.BUILTIN;
    type |= isPureFunctionWithTitle && TestType.FUNCTION_WITH_TITLE
    type |= isPureFunctionWithOnly && TestType.FUNCTION_WITH_ONLY;
    type |= isPureFunctionWithCases && TestType.FUNCTION_WITH_CASES;

    const testCases = isPureFunctionWithCases
      ? getPureFunctionCases(value, options)
      : [];

    const boundFn = isPureFunctionWithCases
      ? value
      : getTestValueAsFunction(
        field.groupName,
        value,
        options
      );

    context.tests.push(
      new Test(
        field.groupName,
        // name
        isPureFunctionWithTitle
          ? getTitle(value, options)
          : name,
        // value
        boundFn,
        new TestType(type),
        testCases,
        // only expression
        isPureFunctionWithOnly
      )
    );

    return true;
  }

  return false;
}

/**
 * 
 * @param {string} groupName 
 * @param {Function} testFn 
 * @param {TestParserOptions} options 
 * @returns 
 */
function getTestValueAsFunction(groupName, testFn, options) {
  // some test runners inspect the test fn length
  // when the length is > 0 they then pass their utility args to the test fn
  // i.e. a done() callback
  if (testFn.length === 0) {
    return () => {
      const thisArg = options.testContextLookup(groupName);
      return testFn.call(thisArg);
    }
  } else if (testFn.length === 1) {
    // ensures the length of the function is at least 1
    return (first, ...args) => {
      const thisArg = options.testContextLookup(groupName);
      return testFn.call(thisArg, first, ...args);
    }
  } else {
    // ensures the length of the function is at least 2
    return (first, second, ...args) => {
      const thisArg = options.testContextLookup(groupName);
      return testFn.call(thisArg, first, second, ...args);
    }
  }
}