import { keywords } from '../../parser/keywords.js';
import { ClassInstanceField, Test, TestType } from '../../parser/models/all.js';
import { hasOnlyDecorator } from '../../parser/utils/testOnlyUtils.js';
import { hasTitleDecorator } from '../../parser/utils/testTitleUtils.js';
import { PreProcessContext } from '../models/preProcessContext.js';

/**
 * 
 * @param {ClassInstanceField} field 
 * @param {number} index 
 * @param {PreProcessContext} context 
 * @returns {boolean}
 */
export function processClassInstFunction(field, index, context) {
  const { name, value } = field;

  if (field instanceof ClassInstanceField && field.isFunction) {
    const isClassInstBuiltInFunction = context.isBuiltInFunction(name);

    const isClassInstFunctionWithOnly = isClassInstBuiltInFunction === false
      && hasOnlyDecorator(field.value);

    const isClassInstFunctionWithCases = isClassInstBuiltInFunction === false
      && field.hasCasesDecorator;

    const isClassInstFunctionWithTitle = hasTitleDecorator(field.value);

    const onlyExpression = isClassInstFunctionWithOnly
      ? value[keywords.only]
      : ""

    const testCases = isClassInstFunctionWithCases
      ? value[keywords.cases].slice().reverse()
      : [];

    let type = TestType.CLASS_INST_FUNCTION;
    type |= isClassInstBuiltInFunction && TestType.BUILTIN_FUNCTION;
    type |= isClassInstFunctionWithOnly && TestType.FUNCTION_WITH_ONLY;
    type |= isClassInstFunctionWithCases && TestType.FUNCTION_WITH_CASES;
    type |= isClassInstFunctionWithTitle && TestType.FUNCTION_WITH_TITLE

    context.tests.push(
      new Test(
        field.groupName,
        // name or title
        isClassInstFunctionWithTitle
          ? value[keywords.title]
          : name,
        // value
        value.bind(field.classInstance),
        // type
        new TestType(type),
        // test cases
        testCases,
        // only expression
        onlyExpression
      )
    );

    return true;
  }

  return false;
}