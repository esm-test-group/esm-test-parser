import { TestParserOptions } from '../options/testParserOptions.js';
import { TestCollection } from '../parser/models/testCollection.js';
import { PostProcessContext } from './models/postProcessContext.js';
import * as TestProcessors from './post/all.js';

const postProcessorOrder = [
  "processHasOnly",
  "processClassInstTestCases",
  "processTestCases",
];

/**
 * @typedef {(
 *    test: Test,
 *    index: number,
 *    context: PostProcessContext
 * ) => boolean} PostProcessor
 */

/**
 * @type {PostProcessor[]}
 */
const postProcessors = postProcessorOrder.map(
  pk => TestProcessors[pk]
);

/**
 * 
 * @param {object} fields 
 * @param {TestCollection} preTests
 * @param {TestParserOptions} options
 * @returns {TestCollection}
 */
export function executePostProcessing(moduleToParse, preTests, options) {
  const postTests = new TestCollection();

  const postProcessContext = new PostProcessContext(
    moduleToParse,
    options,
    preTests,
    postTests
  );

  preTests.forEach((test, testIndex) => {
    for (let index = 0; index < postProcessors.length; index++) {
      const processor = postProcessors[index];
      const processResult = processor(test, testIndex, postProcessContext);

      if (processResult === true) {
        return;
      }
    }

    postTests.push(test);
  });

  return postTests;
}
