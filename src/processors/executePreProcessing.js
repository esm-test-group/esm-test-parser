import { TestParserOptions } from '../options/testParserOptions.js';
import { ClassInstanceField, ObjectField, TestCollection } from '../parser/models/all.js';
import { PreProcessContext } from './models/preProcessContext.js';
import * as FieldProcessors from './pre/all.js';

// ordered field processors
const preProcessorOrder = [
  "processDefaultExport",
  "processModule",
  "processClassDef",
  "processClassInstance",
  "processClassInstFunction",
  "processFunction",
  "processFunctionWithCasesArray",
  "processOnlyField",
  "processObject"
];

/**
 * @typedef {(
 *    test: ObjectField | ClassInstanceField, 
 *    index: number, 
 *    context: PreProcessContext
 * ) => boolean} PreProcessor
 */

/**
 * @type {PreProcessor[]}
 */
const preProcessors = preProcessorOrder.map(pk => FieldProcessors[pk]);

/**
 * 
 * @param {(ObjectField | ClassInstanceField)[]} fields
 * @param {TestParserOptions} options
 * @returns {TestCollection}
 */
export function executePreProcessing(fields, options) {
  const tests = new TestCollection();

  for (let fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
    const field = fields[fieldIndex];
    const preProcessContext = new PreProcessContext(
      tests,
      options
    );

    for (let index = 0; index < preProcessors.length; index++) {
      const processor = preProcessors[index];
      const testProp = processor(field, fieldIndex, preProcessContext);
      if (testProp === true) {
        break;
      }
    }
  }

  // prevent unprocessed fields from being included
  return tests.filter(field => field !== undefined);
}