import { Test, TestCollection, TestType } from '../../parser/models/all.js';

/**
 * 
 * @param {Test} test
 * @param {number} index
 * @param {TestCollection} tests
 */
export function applyOnlyExpression(test, index, tests) {
  let onlyCount = 0;
  const { groupName, onlyExpression } = test;

  if (onlyExpression === '*') {
    onlyCount = tests.length;
  } else if (typeof (+onlyExpression) === 'number') {
    onlyCount = index + (+onlyExpression);
  }

  onlyCount = Math.min(onlyCount, tests.length);
  for (let i = index; i < onlyCount; i++) {
    const nextTest = tests[i];

    // ensure the nextTest is in the same group
    if (nextTest.groupName != groupName) continue;

    // don't process built in functions
    if (nextTest.type.has(TestType.BUILTIN_FUNCTION)) {
      continue;
    }

    // set type if it isn't already set
    if (nextTest.isOnly === false) {
      nextTest.type.value |= TestType.HAS_ONLY
    }
  }
}

/**
 * 
 * @param {string} groupName
 * @param {import('../../options/testParserOptions.js').ContextLookUp} testContextLookup
 * @param {Function} testFn
 * @param {Array<any>} testCaseArgs
 * @returns {Function}
 */
export function createTestCaseFn(groupName, testContextLookup, testFn, testCaseArgs) {
  // some test runners inspect the test fn length
  // when the length is > 0 they then pass their utility args to the test fn
  // i.e. a done() callback
  if (testFn.length <= testCaseArgs.length) {
    return () => {
      const thisArg = testContextLookup(groupName);
      return testFn.call(thisArg, ...testCaseArgs);
    }
  } else if ((testFn.length - testCaseArgs.length) === 1) {
    // ensures the length of the function is at least 1
    return (first, ...args) => {
      const thisArg = testContextLookup(groupName);
      return testFn.call(thisArg, ...testCaseArgs, first, ...args);
    }
  } else {
    // ensures the length of the function is at least 2
    return (first, second, ...args) => {
      const thisArg = testContextLookup(groupName);
      return testFn.call(thisArg, ...testCaseArgs, first, second, ...args);
    }
  }
}