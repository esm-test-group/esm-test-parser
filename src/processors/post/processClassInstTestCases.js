import { TestParserOptions } from '../../options/testParserOptions.js';
import { Test, TestCollection, TestType } from '../../parser/models/all.js';
import { PostProcessContext } from '../models/postProcessContext.js';

/**
 * 
 * @param {Test} test
 * @param {index} index
 * @param {PostProcessContext} context
 * @returns {boolean}
 */
export function processClassInstTestCases(test, index, context) {

  if (test.type.has(TestType.CLASS_INST_FUNCTION_WITH_CASES)) {
    const testCaseProperties = createClassInstTestCases(
      test,
      context.moduleToParse,
      context.options
    );

    context.postTests.push.apply(context.postTests, testCaseProperties);

    return true;
  }

  return false;
}

/**
 * 
 * @param {Test} test
 * @param {object} classInstance
 * @param {TestParserOptions} options
 * @returns {TestCollection}
 */
function createClassInstTestCases(test, classInstance, options) {
  const { name, value: testFn, testCases } = test;

  const results = [];
  testCases.forEach(
    (testCases, testCaseIndex) => {
      const testCaseArgs = [...testCases, testCaseIndex];
      const testCaseFn = testFn.bind(classInstance, ...testCaseArgs);
      const newTestCase = new Test(
        test.groupName,
        options.testCaseTitleTemplate(name, testCases, testCaseIndex),
        testCaseFn,
        test.type,
        test.testCases,
        test.onlyExpression
      );
      results.push(newTestCase);
    }
  )

  return results;
}
