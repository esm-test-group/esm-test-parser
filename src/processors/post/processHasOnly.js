import { TestType } from '../../parser/models/testType.js';
import { isOnlyField } from '../../parser/utils/testOnlyUtils.js';
import { PostProcessContext } from '../models/postProcessContext.js';
import { applyOnlyExpression } from './postProcessUtils.js';

/**
 * 
 * @param {Test} test
 * @param {number} index
 * @param {PostProcessContext} context 
 * @returns {boolean}
 */
export function processHasOnly(test, index, context) {
  if (test.type.has(TestType.HAS_ONLY) === false) {
    return false;
  }

  if (test.type.is(TestType.OBJECT_WITH_ONLY) === false) {

    applyOnlyExpression(
      test,
      index,
      context.preTests
    );

    return false;
  }

  // check if this is an only field
  if (isOnlyField(test.name, context.options) === false) return false;

  // get the next object or function test
  const nextIndex = index + 1;
  const nextTest = context.nextHasEither(
    nextIndex,
    TestType.OBJECT,
    TestType.FUNCTION
  );

  if (nextTest !== null) {
    nextTest.groupName = test.groupName;
    nextTest.type.value |= TestType.HAS_ONLY;
    nextTest.onlyExpression = test.onlyExpression;

    applyOnlyExpression(
      test,
      nextIndex,
      context.preTests
    );
  }

  // prevent the only field from further processing
  return true;
}