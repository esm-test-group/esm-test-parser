import { TestParserOptions } from '../../options/testParserOptions.js';
import { Test, TestCollection, TestType } from '../../parser/models/all.js';
import { PostProcessContext } from '../models/postProcessContext.js';
import { createTestCaseFn } from './postProcessUtils.js';

/**
 * 
 * @param {Test} test
 * @param {index} index
 * @param {PostProcessContext} context
 * @returns {boolean}
 */
export function processTestCases(test, index, context) {
  if (test.type.has(TestType.FUNCTION_WITH_CASES)) {
    const testCaseProperties = createTestCases(test, context.options);
    context.postTests.push.apply(context.postTests, testCaseProperties);
    return true;
  }

  return false;
}

/**
 * 
 * @param {Test} test
 * @param {object} moduleToParse
 * @param {TestParserOptions} options
 * @returns {TestCollection}
 */
function createTestCases(test, options) {
  const {
    name,
    value: testFn,
    groupName,
    testCases
  } = test;

  const testContextLookup = options.testContextLookup;

  const results = [];
  testCases.forEach(
    (testCases, testCaseIndex) => {
      const testCaseArgs = [...testCases, testCaseIndex];

      const testCaseFn = createTestCaseFn(
        groupName,
        testContextLookup,
        testFn,
        testCaseArgs
      );

      const newTestCase = new Test(
        test.groupName,
        options.testCaseTitleTemplate(name, testCases, testCaseIndex),
        testCaseFn,
        test.type,
        test.testCases,
        test.onlyExpression
      );

      results.push(newTestCase);
    }
  )

  return results;
}
