import { throwNotInstance } from '@esm-test/guards';
import { TestCollection } from '../../parser/models/testCollection.js';
import { TestParserOptions } from '../../options/testParserOptions.js';

export class PreProcessContext {

  /**
   * @param {TestCollection} tests 
   * @param {TestParserOptions} options 
   */
  constructor (tests, options) {
    throwNotInstance("tests", tests, TestCollection);
    throwNotInstance("options", options, TestParserOptions);

    this.tests = tests;
    this.options = options;
  }

  /**
   * @type {TestCollection}
   */
  tests;

  /**
   * @type {TestParserOptions}
   */
  options;

  /**
   * 
   * @param {string} name 
   * @returns {boolean} 
   */
  isBuiltInFunction(name) {
    return this.options.builtInFunctions.includes(name);
  }

}