import { throwNotInstance, throwNull, throwUndefined } from '@esm-test/guards';
import { TestParserOptions } from '../../options/testParserOptions.js';
import { TestCollection, TestType } from '../../parser/models/all.js';
import { isClassDefinition } from '../../parser/utils/classUtils.js';
import { isModule } from '../../parser/utils/moduleUtils.js';

export class PostProcessContext {

  /**
   * 
   * @param {object} moduleToParse 
   * @param {TestParserOptions} options 
   * @param {TestCollection} preTests 
   * @param {TestCollection} postTests 
   */
  constructor (moduleToParse, options, preTests, postTests) {
    throwUndefined("moduleToParse", moduleToParse)
      || throwNull("moduleToParse", moduleToParse)
      || isModule(moduleToParse)
      || isClassDefinition(moduleToParse)
      || throwNotInstance("moduleToParse", moduleToParse, Object);

    throwNotInstance("options", options, TestParserOptions);
    throwNotInstance("preTests", preTests, TestCollection);
    throwNotInstance("postTests", postTests, TestCollection);

    this.moduleToParse = moduleToParse;
    this.options = options;
    this.preTests = preTests;
    this.postTests = postTests;
  }

  /**
   * @type {object}
   */
  moduleToParse;

  /**
   * @type {TestParserOptions}
   */
  options;

  /**
   * @type {TestCollection}
   */
  preTests;

  /**
   * @type {TestCollection}
   */
  postTests;

  /**
   * 
   * @param {number} startIndex 
   * @param  {TestType[]} type 
   * @returns {Test | null}
   */
  nextHasEither(startIndex, ...type) {
    const props = this.preTests;
    for (let index = startIndex; index < props.length; index++) {
      const prop = props[index];
      if (prop.type.hasEither(...type)) {
        return prop;
      }
    }
    return null;
  }

}