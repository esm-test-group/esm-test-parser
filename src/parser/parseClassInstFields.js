import { keywords } from '../parser/keywords.js';
import { ClassInstanceField } from './models/classInstanceField.js';

const ignoreClassInstFields = [
  "length",
  "prototype",
  "name",
  keywords.title,
  keywords.only
];

/**
 * @param {string} groupName
 * @param {object} classInstance
 * @returns {ClassInstanceField[]}
 */
export function parserClassInstFields(groupName, classInstance) {
  // get the class prototype
  const classProto = Reflect.getPrototypeOf(classInstance);

  // extract the class fields
  const fields = forEachClassProto(classProto)
    .filter(x => ignoreClassInstFields.includes(x) === false)
    .map(
      name => new ClassInstanceField(
        groupName,
        name,
        classProto[name],
        classInstance
      )
    );

  return fields;
}

/**
 * @param {object} classProto
 * @returns {Array<string>}
 */
function forEachClassProto(classProto) {
  const keys = [];
  const ignoreProtoFields = x => x !== 'constructor' && keys.indexOf(x) === -1;

  // iterate all class prototype fields (including class extended prototype fields)
  do {
    keys.push.apply(
      keys,
      Reflect.ownKeys(classProto).filter(ignoreProtoFields)
    );
    classProto = Reflect.getPrototypeOf(classProto);
  } while (classProto && classProto !== Object.prototype);

  return keys;
}