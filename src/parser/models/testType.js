export class TestType {

  /**
   * @param {number} type 
   */
  constructor (type) {
    if (typeof type !== 'number') {
      throw new Error(`${TestType.name} must be a number. Instead saw ${type}`);
    }

    if (type < MIN_FLAG || type > MAX_FLAG) {
      throw new Error(`${TestType.name} value out of range. Instead saw ${type}`);
    }

    this.value = type;
  }

  /**
   * @type {number}
   */
  value;

  /**
   * @param {number} testFlag 
   * @returns {boolean}
   */
  has(testFlag) {
    return (this.value & testFlag) === testFlag;
  }

  /**
   * @param {number[]} testFlags 
   * @returns {boolean}
   */
  hasEither(...testFlags) {
    for (let i = 0; i < testFlags.length; i++) {
      if (this.has(testFlags[i])) return true;
    }
    return false;
  }

  /**
   * @param {number} testFlag 
   * @returns {boolean}
   */
  is(testFlag) {
    return this.value === testFlag;
  }

  /**
 * @param {number[]} testFlags 
 * @returns {boolean}
 */
  isEither(...testFlags) {
    for (let i = 0; i < testFlags.length; i++) {
      if (this.is(testFlags[i])) return true;
    }
    return false;
  }

  toString() {
    const typeValue = this.value;
    return Object.keys(TestType)
      .filter(key => TestType[key] === typeValue)
      .join('|');
  }

  // binary flags for composing test property types
  static HAS_ONLY = 1;
  static HAS_CASES = 2;
  static HAS_TITLE = 4;
  static EXPORT_DEFAULT = 8;

  static MODULE = 16;
  static MODULE_WITH_TITLE = TestType.MODULE | TestType.HAS_TITLE;

  // objects
  static OBJECT = 32;
  static OBJECT_WITH_ONLY = TestType.OBJECT | TestType.HAS_ONLY;

  // functions
  static FUNCTION = 64;
  static FUNCTION_WITH_CASES = TestType.FUNCTION | TestType.HAS_CASES;
  static FUNCTION_WITH_ONLY = TestType.FUNCTION | TestType.HAS_ONLY;
  static FUNCTION_WITH_CASES_ONLY = TestType.FUNCTION_WITH_CASES | TestType.HAS_ONLY;
  static FUNCTION_WITH_CASES_TITLE = TestType.FUNCTION_WITH_CASES | TestType.HAS_TITLE;
  static FUNCTION_WITH_TITLE = TestType.FUNCTION | TestType.HAS_TITLE;
  static FUNCTION_WITH_TITLE_ONLY = TestType.FUNCTION_WITH_TITLE | TestType.HAS_ONLY;

  // built in
  static BUILTIN = 128;
  static BUILTIN_FUNCTION = TestType.BUILTIN | TestType.FUNCTION;

  // class definitions
  static CLASS_DEF = 256;
  static CLASS_DEF_EXPORT_DEFAULT = TestType.CLASS_DEF | TestType.EXPORT_DEFAULT;
  static CLASS_DEF_WITH_ONLY = TestType.CLASS_DEF | TestType.HAS_ONLY;
  static CLASS_DEF_WITH_TITLE = TestType.CLASS_DEF | TestType.HAS_TITLE;

  // class instances
  static CLASS_INST = 512;
  static CLASS_INST_FUNCTION = TestType.CLASS_INST | TestType.FUNCTION;
  static CLASS_INST_FUNCTION_WITH_CASES = TestType.CLASS_INST | TestType.FUNCTION_WITH_CASES;
  static CLASS_INST_FUNCTION_WITH_ONLY = TestType.CLASS_INST | TestType.FUNCTION_WITH_ONLY;
  static CLASS_INST_FUNCTION_WITH_ONLY_CASES = TestType.CLASS_INST | TestType.FUNCTION_WITH_CASES_ONLY;
  static CLASS_INST_FUNCTION_WITH_TITLE = TestType.CLASS_INST | TestType.FUNCTION_WITH_TITLE;
  static CLASS_INST_BUILTIN_FUNCTION = TestType.CLASS_INST | TestType.BUILTIN_FUNCTION;
}

const MIN_FLAG = TestType.HAS_ONLY;
const MAX_FLAG = TestType.CLASS_INST_BUILTIN_FUNCTION;