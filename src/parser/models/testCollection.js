import { TestType } from "./testType.js";
import { Test } from "./test.js";

/**
 * @typedef {(a: Test, b: Test) => number} CompareMethod
 */

/**
 * @extends {Array<Test>}
 * @type {Array<Test>}
 */
export class TestCollection extends Array {

  constructor (initialArray) {
    super();
    if (initialArray && initialArray.length > 0) {
      this.push.apply(this, initialArray);
    }
  }

  /**
   * @param {CompareMethod} compareMethod
   * @returns {Array<Test>}
   */
  sortBy(compareMethod) {
    return this.slice().sort(compareMethod);
  }

  /**
   * @param {TestType} isType
   * @returns {Array<Test>}
   */
  filterIs(isType) {
    return this.filter(x => x.type.is(isType));
  }

  /**
   * @param {TestType[]} isType
   * @returns {Array<Test>}
   */
  filterIsEither(...isType) {
    return this.filter(x => x.type.isEither(...isType));
  }

  /**
   * @param {TestType} hasType
   * @returns {Array<Test>}
   */
  filterHas(hasType) {
    return this.filter(x => x.type.has(hasType));
  }

  /**
   * @param {TestType[]} hasType
   * @returns {Array<Test>}
   */
  filterHasEither(...hasType) {
    return this.filter(x => x.type.hasEither(...hasType));
  }

}

/**
 * @enum {string}
 */
export const SortByEnum = {
  /**
   * Sorts groups and tests
   * @property {string}
   */
  "all": "all",
  /**
   * Sort groups only
   * @type {string}
   */
  "groupsOnly": "groupsOnly",
  /**
   * Sort tests only
   * @type {string}
   */
  "testsOnly": "testsOnly",
  /**
   * No sort
   * @type {string}
   */
  "none": "none"
}

export const SortByMethod = {
  /**
   * @property {CompareMethod}
   */
  groupsOnly: function (a, b) {
    // only process groups
    if (a.isGroup === false && b.isGroup === false) return 0;
    if (a.isGroup === false) return 1;
    if (b.isGroup === false) return -1;

    // sort by group name first
    const groupComare = stringCompare(a.groupName, b.groupName);
    if (groupComare !== 0) return groupComare;

    // sort by name when in the same group
    return stringCompare(a.name, b.name);
  },
  /**
   * @property {CompareMethod}
   */
  testsOnly: function (a, b) {
    // only process tests
    if (a.isGroup && b.isGroup) return 0;
    if (a.isGroup) return -1;
    if (b.isGroup) return 1;

    // sort by test name
    return stringCompare(a.name, b.name);
  },
  /**
   * @property {CompareMethod}
   */
  all: function (a, b) {
    const compare = SortByMethod.groupsOnly(a, b)
    if (compare === 0) return SortByMethod.testsOnly(a, b);
    return compare;
  }
}

/**
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function stringCompare(a, b) {
  const al = a.toLowerCase();
  const bl = b.toLowerCase();
  if (al < bl) return -1;
  if (al > bl) return 1;
  return 0;
}