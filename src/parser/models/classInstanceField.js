import { throwNull, throwUndefined } from '@esm-test/guards';
import { ObjectField } from './objectField.js';

export class ClassInstanceField extends ObjectField {

  /**
   * @param {string} groupName 
   * @param {string} name 
   * @param {any} value 
   * @param {object} classInstance 
   */
  constructor (groupName, name, value, classInstance) {
    super(groupName, name, value)

    throwUndefined("classInstance", classInstance)
      || throwNull("classInstance", classInstance);

    this.classInstance = classInstance;
  }

  /**
   * @type {object}
   */
  classInstance;

}