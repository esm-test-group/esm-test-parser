import {
  throwNotInstance,
  throwNotType,
  throwNull,
  throwUndefined
} from '@esm-test/guards';
import { getChildGroupName } from '../../processors/pre/preProcessUtils.js';
import { TestType } from './testType.js';

export class Test {

  /**
   * @param {string} groupName 
   * @param {string} name 
   * @param {any} value 
   * @param {TestType} type 
   * @param {[]} testCases 
   * @param {string} onlyExpression
   */
  constructor (groupName, name, value, type, testCases, onlyExpression) {
    throwNotType("groupName", groupName, "string");
    throwNotType("name", name, "string");

    throwUndefined("value", value)
      || throwNull("value", value);

    throwNotInstance("type", type, TestType);
    throwNotInstance("testCases", testCases, Array);

    throwUndefined("onlyExpression", onlyExpression)
      || throwNull("onlyExpression", onlyExpression);

    this.groupName = groupName;
    this.name = name;
    this.value = value;
    this.type = type;
    this.testCases = testCases;
    this.onlyExpression = onlyExpression;
  }

  /**
   * @type {string}
   */
  groupName;

  /**
   * @type {string}
   */
  name;

  /**
   * @type {any}
   */
  value;

  /**
   * @type {TestType}
   */
  type;

  /**
   * @type {Array<any>}
   */
  testCases;

  /**
   * @type {boolean}
   */
  get isOnly() {
    return this.type.has(TestType.HAS_ONLY);
  }

  /**
   * The expression used to run tests from the `only` 
   * statement in the same test group
   * 
   * |expression||
   * |-|-|
   * |true|runs a single test
   * |numeric|number of tests to run
   * |'*'|all tests
   * 
   * @type {string}
   */
  onlyExpression

  /**
   * @type {boolean}
   */
  get isGroup() {
    return this.type.isEither(
      TestType.EXPORT_DEFAULT,
      TestType.MODULE,
      TestType.OBJECT,
      TestType.OBJECT_WITH_ONLY,

      TestType.CLASS_DEF,
      TestType.CLASS_DEF_EXPORT_DEFAULT,
      TestType.CLASS_DEF_WITH_TITLE,
      TestType.CLASS_DEF_WITH_ONLY,

      TestType.CLASS_INST
    );
  }

  /**
   * @type {string}
   */
  get key() {
    return getChildGroupName(this.groupName, this.name);
  }

}