import { throwNotType, throwNull, throwUndefined } from '@esm-test/guards';
import { keywords } from '../keywords.js';

export class ObjectField {

  /**
   * @param {string} groupName
   * @param {string} name
   * @param {any} value
   */
  constructor (groupName, name, value) {
    throwNotType("groupName", groupName, "string");
    throwNotType("name", name, "string");

    throwUndefined("value", value)
      || throwNull("value", value);

    this.groupName = groupName;
    this.name = name;
    this.value = value;
  }

  /**
   * @type {string}
   */
  groupName;

  /**
   * @type {string}
   */
  name;

  /**
   * @type {any}
   */
  value;

  /**
   * @type {boolean}
   */
  get isDefaultExport() {
    return this.name == keywords.defaultExport;
  }

  /**
   * @type {boolean}
   */
  get isFunction() {
    return this.value instanceof Function;
  }

  /**
   * @type {Array<any>}
   */
  get isFunctionWithCases() {
    const value = this.value;
    return value instanceof Array
      && value.length > 0
      && value[value.length - 1] instanceof Function;
  }

  /**
   * @type {boolean}
   */
  get hasCasesDecorator() {
    return this.value[keywords.cases] instanceof Array;
  }

}