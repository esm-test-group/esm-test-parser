import { keywords } from "../keywords.js";

/**
 * @param {string} name
 * @param {TestParserOptions} options
 * @returns {boolean}
 */
export function isOnlyField(name, options) {
  return name === keywords.only
    || (options.parserFlags.allowLiteralOnly && name === "only");
}

/**
 * @param {object} value
 * @param {TestParserOptions} options
 * @returns {boolean}
 */
export function hasOnly(value, options) {
  return Object.hasOwn(value, keywords.only)
    || (options.parserFlags.allowLiteralOnly && Object.hasOwn(value, "only"))
}

/**
 * @param {object} value
 * @returns {boolean}
 */
export function hasOnlyDecorator(value) {
  return Object.hasOwn(value, keywords.only)
    && isOnlyExpressionSupported(value[keywords.only]);
}


/**
 * @param {string | number | void} expression 
 * @returns {boolean}
 */
export function isOnlyExpressionSupported(expression) {
  const isNumber = typeof (+expression) === 'number' && +expression > 0;
  const isStar = expression === '*';

  return isNumber || isStar;
}