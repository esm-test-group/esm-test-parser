import { keywords } from "../keywords.js";

/**
 * @param {object} value
 * @returns {boolean}
 */
export function isModule(value) {
  return value[Symbol.toStringTag] === keywords.moduleTag
}