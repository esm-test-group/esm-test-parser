/**
 * @param {object} value
 * @returns {boolean}
 */
export function isClassDefinition(value) {
  return value.prototype !== undefined &&
    value.prototype.constructor !== undefined &&
    value.prototype.constructor.toString().startsWith("class");
}

/**
 * @param {object} value
 * @returns {boolean}
 */
export function isClassInstance(value) {
  const proto = Object.getPrototypeOf(value);
  return proto.constructor
    && proto.constructor.toString().startsWith("class");
}