import { TestParserOptions } from "../../options/testParserOptions.js";
import { keywords } from "../keywords.js";

/**
 * @param {object} value
 * @param {TestParserOptions} options
 * @returns {boolean}
 */
export function hasTitle(value, options) {
  return Object.hasOwn(value, keywords.title)
    || (options.parserFlags.allowLiteralTitle && Object.hasOwn(value, "title"))
}

/**
 * @param {object} value
 * @param {TestParserOptions} options
 * @returns {boolean}
 */
export function getTitle(value, options) {
  return value[keywords.title]
    || (options.parserFlags.allowLiteralTitle && value["title"]);
}

/**
 * @param {object} value
 * @returns {boolean}
 */
export function hasTitleDecorator(value) {
  return typeof value[keywords.title] === 'string';
}