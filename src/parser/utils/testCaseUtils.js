import { keywords } from "../keywords.js";

/**
 * @param {string} title
 * @param {Array<any[]>} testCases
 * @param {number} caseIndex
 */
export function simpleReplace(title, testCases, caseIndex) {
  let newTitle = title.replaceAll("$i", caseIndex + 1);
  testCases.forEach(
    (arg, index) => newTitle = newTitle.replaceAll(`$${index + 1}`, arg)
  );
  return newTitle;
}

/**
 * @param {object} value
 * @param {TestParserOptions} options
 * @returns {boolean}
 */
export function hasPureFunctionCases(value, options) {
  return Object.hasOwn(value, keywords.cases)
    || (options.parserFlags.allowLiteralCases && Object.hasOwn(value, "testCases"));
}

/**
 * @param {Array<any[] | Function>} value
 * @returns {Array<any[]>}
 */
export function getFunctionTestCases(value) {
  const testCases = value.slice(0, value.length - 1);
  return mapTestCases(testCases);
}

/**
 * @param {object} value
 * @param {TestParserOptions} options
 * @returns {Array<any[]>}
 */
export function getPureFunctionCases(value, options) {
  const testCases = value[keywords.cases]
    || (options.parserFlags.allowLiteralCases && value["testCases"]);

  return mapTestCases(testCases);
}

/**
 * @param {Array<any[] | any>} testCases 
 * @returns {Array<any[]>}
 */
export function mapTestCases(testCases) {
  return testCases.map(x => {
    // check if we have an array already
    if (x instanceof Array) return x;

    // return single cases as an array
    return [x];
  });
}