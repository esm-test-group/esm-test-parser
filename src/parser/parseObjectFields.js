import { ObjectField } from './models/objectField.js';

/**
 * @param {string} groupName 
 * @param {object} moduleToParse 
 * @returns {ObjectField[]}
 */
export function parseObjectFields(groupName, moduleToParse) {
  const fields = [];
  fields.push.apply(
    fields,
    Object.keys(moduleToParse)
      .map(name => new ObjectField(groupName, name, moduleToParse[name])
      )
  );

  return fields;
}