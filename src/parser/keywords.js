const only = "__test.only__";
const title = "__test.title__";
const cases = "__test.cases__";
const defaultExport = "default";
const moduleTag = "Module";

export const keywords = {
  only,
  title,
  cases,
  defaultExport,
  moduleTag
}