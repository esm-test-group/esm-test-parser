import { throwNotComplexObject, throwNotType } from '@esm-test/guards';

const defaultParserFlags = {
  allowLiteralOnly: true,
  allowLiteralTitle: true,
  allowLiteralCases: true
};

export class TestParserFlags {

  /**
   * @param {TestParserFlags|Object} parserFlags 
   */
  constructor (parserFlags) {
    throwNotComplexObject("parserFlags", parserFlags)

    Object.assign(this, defaultParserFlags, parserFlags);

    throwNotType("allowLiteralOnly", this.allowLiteralOnly, "boolean");
    throwNotType("allowLiteralTitle", this.allowLiteralTitle, "boolean");
    throwNotType("allowLiteralCases", this.allowLiteralCases, "boolean");
  }

  /**
   * @type {boolean}
   */
  allowLiteralOnly;

  /**
   * @type {boolean}
   */
  allowLiteralTitle;

  /**
   * @type {boolean}
   */
  allowLiteralCases;

}