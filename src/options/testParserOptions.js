import {
  throwNotComplexObject,
  throwNotInstance,
  throwNotObjectKey,
  throwNotType
} from '@esm-test/guards';
import { SortByEnum } from '../parser/models/testCollection.js';
import { simpleReplace } from '../parser/utils/testCaseUtils.js';
import { TestParserFlags } from './testParserFlags.js';

/**
 * @typedef {(groupName: string) => any | undefined} ContextLookUp
 */

/**
 * @type {TestParserOptions}
 */
const defaultOptions = {
  builtInFunctions: [
    'beforeAll',
    'beforeEach',
    'afterAll',
    'afterEach'
  ],
  classContextPropertyName: 'suiteContext',
  testContextLookup: (groupName) => { },
  testCaseTitleTemplate: simpleReplace,
  parserFlags: new TestParserFlags({}),
  sort: 'none'
}

export class TestParserOptions {

  /**
   * @param {TestParserOptions|Object} options 
   */
  constructor (options) {
    throwNotComplexObject("options", options)

    Object.assign(this, defaultOptions, options);

    throwNotInstance("builtInFunctions", this.builtInFunctions, Array);
    throwNotType(
      "classContextPropertyName",
      this.classContextPropertyName,
      "string"
    );
    throwNotInstance("testContextLookup", this.testContextLookup, Function);
    throwNotInstance("testCaseTitleTemplate", this.testCaseTitleTemplate, Function);
    throwNotInstance("parserFlags", this.parserFlags, TestParserFlags);
    throwNotObjectKey("sort", SortByEnum, this.sort);
  }

  /**
   * Names used to mark tests as builtin.
   * The default value is ["beforeAll", "beforeEach", "afterEach", "afterAll"]
   * @type {Array<string>}
   */
  builtInFunctions;

  /**
   * Sets the property name used for accessing the context inside class tests
   * @default 'suiteContext'
   *
   * ```ts
   *  class Tests {
   * 
   *    test1() {
          console.log(this.suiteContext)
        }

      }
   * ```
   * @type {string | undefined}
   */
  classContextPropertyName;

  /**
   * Sets the context instance used for accessing the context inside tests
   * @default {}
   *
   * @type {ContextLookUp}
   */
  testContextLookup;

  /**
   * Test case title template function.
   * 
   * The default function will replace $num args with test case argument values
   * 
   * Example
   * - test case args [[5, 10]]
   * - title input `expected $1 to be $2 (case $i)`
   * - title output `expected 5 to be 10 (case 1)`
   * @type {(title: string, testCases: Array<any[]>, caseIndex: number) => void}
   */
  testCaseTitleTemplate;

  /**
   * @type {TestParserFlags | undefined}
   */
  parserFlags;

  /**
   * Can be either
   * - "none" (default)
   * - "groupsOnly"
   * - "testsOnly"
   * - "all"
   * @type {SortByEnum}
   */
  sort;
}