import { SortByMethod, TestCollection } from './parser/models/testCollection.js';
import { TestParserOptions } from './options/testParserOptions.js';
import { parseObjectFields } from './parser/parseObjectFields.js';
import { executePostProcessing } from './processors/executePostProcessing.js';
import { executePreProcessing } from './processors/executePreProcessing.js';

/**
 * @param {object} moduleToParse
 * @param {TestParserOptions|Object} options
 * @returns {TestCollection}
 */
export function extractTestsFromModule(moduleToParse, options) {
  const parserOptions = new TestParserOptions(options);

  // pre process module fields
  const fields = parseObjectFields("root", moduleToParse);
  const tests = executePreProcessing(fields, parserOptions);

  // post process tests
  const testCollection = executePostProcessing(
    moduleToParse,
    tests,
    parserOptions
  );

  // get the sort method
  const sortMethod = SortByMethod[parserOptions.sort]

  // return sorted collection
  return sortMethod
    ? testCollection.sortBy(sortMethod)
    : testCollection;
}