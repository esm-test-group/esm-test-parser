# Esm test parser

[![The ISC license](https://img.shields.io/badge/license-ISC-orange.png?color=blue&style=flat-square)](http://opensource.org/licenses/ISC)
[![NPM version](https://img.shields.io/npm/v/esm-test-parser.svg)](https://www.npmjs.org/package/esm-test-parser)
[![NPM downloads](https://img.shields.io/npm/dm/esm-test-parser.svg)](https://npmjs.org/package/esm-test-parser "View this project on NPM")

[![BuyMeACoffee](https://www.buymeacoffee.com/assets/img/custom_images/purple_img.png)](https://www.buymeacoffee.com/peterf)

> This library is not a test runner. It's a utility for creating test runner plugins.

This is a library for converting esm modules in to tests.
It analyses a given javascript module\object and returns an array of tests which can then be added to a test runner.

```sh
npm install esm-test-parser
```