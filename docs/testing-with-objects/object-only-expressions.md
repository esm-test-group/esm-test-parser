# Object Only Expressions

#### Run a single test using `only: true`

[//js]: ./test/examples/objects/only/objectOnly.example.js[/]
```js
export const ObjectOnlyExample = {

  // runs this test only
  only: true,
  'test 1': function () {

  },

  'This test will not run': function () { throw new Error(this.test.title) },

}
```

#### Run all tests following the only expression by using `only: '*'`

[//js]: ./test/examples/objects/only/objectOnlyStar.example.js[/]
```js
import assert from 'node:assert';

let testsRun;

export const ObjectOnlyStarExample = {

  beforeAll: () => testsRun = 0,

  afterAll: () => assert.equal(testsRun, 3),

  'This test will not run': function () { throw new Error(this.test.title) },

  // runs all the following tests after this statement
  only: '*',

  'test 1': () => testsRun++,

  'test 2': () => testsRun++,

  'test 3': () => testsRun++

}
```

#### Run a specified number of tests from the only expression by using `only: #num`

[//js]: ./test/examples/objects/only/objectOnlyNumber.example.js[/]
```js
import assert from 'node:assert';

let testsRun;

export const ObjectOnlyNumberExample = {

  beforeAll: () => testsRun = 0,

  afterAll: () => assert.equal(testsRun, 2),

  'This test will not run': function () { throw new Error(this.test.title) },

  // runs the 2 tests following this statement
  only: '2',

  // this function will run
  'test 1': () => testsRun++,

  // this function will run
  'test 2': () => testsRun++,

  'This test will not run either': function () { throw new Error(this.test.title) },

}
```