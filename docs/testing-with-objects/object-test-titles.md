# Object Test Titles

#### Using export names as titles for test groups

[//js]: ./test/examples/objects/titles/objectExportTestTitles.example.js[/]
```js
import * as assert from 'assert';

/**
 * Hierarchy output
 * 
 * ObjectExportTestGroupTitle
 *  ✔ property name title
 *  ✔ testWithNoTitle
 */
export const ObjectExportTestGroupTitle = {

  // test titles are taken from the property names.
  // js property names must be unique
  ['property name title']: function () {
    assert.equal(this.test.title, 'property name title');
  },

  testWithNoTitle: function () {
    assert.equal(this.test.title, 'testWithNoTitle')
  }

}
```

#### Using custom titles for test groups

[//js]: ./test/examples/objects/titles/objectCustomTestGroupTitles.example.js[/]
```js
import * as assert from 'assert';
import { test } from 'esm-test-parser';

/**
 * Hierarchy output
 * 
 * Test group title example 1
 *  ✔ test
 */
export const ObjectCustomTestGroupTitle = {

  // set a custom test group title
  [test.title]: "Test group title example 1",

  test: function () {
    assert.equal(this.test.parent.title, "Test group title example 1")
  }

}
```