# Object Test Cases

[//js]: ./test/examples/objects/objectTestCases.example.js[/]
```js
import * as assert from 'assert';

/**
 * Expected output
 * 
 *  ObjectTestCasesExample
 *    ✔ multiple test case arguments (case 1)
 *    ✔ multiple test case arguments (case 2)
 *    ✔ single test case arguments (case 1)
 *    ✔ single test case arguments (case 2)
 *    ✔ case index is 1
 *    ✔ case index is 2
 */

export const ObjectTestCasesExample = {

  // $i in the title will be replaced automatically
  // $i is 1 based. i.e. caseIndex of 0 will be presented as "1"

  // test cases with multiple arguments
  'multiple test case arguments (case $i)': [
    [Math.PI, 3],
    [Math.PI * 2, 6],
    function (testRadians, expectedFloor) {
      const actual = Math.floor(testRadians);
      assert.equal(actual, expectedFloor);
    }
  ],

  // test cases with single arguments without brackets
  // note: single argument arrays still need to be wrapped as [[1,2,3]]
  'single test case arguments (case $i)': [
    "test string",
    new Object(),
    function (testValue) {
      assert.equal(testValue instanceof Array, false);
    }
  ],

  // a caseIndex argument is appended to the end of each test case argument list
  'case index is $i': [
    ['case index is 0', 0],
    ['case index is 1', 1],
    function (testInfo, expectedCaseIndex, caseIndex) {
      assert.equal(caseIndex, expectedCaseIndex);
      assert.equal(this.test.title, `case index is ${caseIndex + 1}`);
    }
  ]

}
```