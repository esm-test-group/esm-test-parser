# Object Test Titles

#### Group using nested objects

> **NOTE** Titles must be unique.
If two titles are the same and your editor\ide doesn't detect the duplication
then only one of those tests will run.

[//js]: ./test/examples/objects/objectTestGroups.example.js[/]
```js
import * as assert from 'assert';

let testsRun = 0;

export const ObjectTestGroupingExample = {

  'test group 1': {

    ['test 1']: function () {
      // mocha test title example
      assert.equal(this.test.parent.title, 'test group 1')
      assert.equal(this.test.title, 'test 1')
      testsRun++
    }

  },

  'test group 2': {

    ['test 1']: function () {
      // mocha test title example
      assert.equal(this.test.parent.title, 'test group 2')
      assert.equal(this.test.title, 'test 1')
      testsRun++
    }

  },

  afterAll: () => assert.equal(testsRun, 2)
}
```