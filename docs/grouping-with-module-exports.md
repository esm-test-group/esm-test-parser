# Grouping with module exports

The follow examples produce this hierarchy
```sh
NamingModuleGroups
  ClassGroupExample1
    ✔ test 1
  ObjectGroupExample1
    ✔ test 1
```

#### You can name a module test group using `export as`

[//js]: ./test/examples/groups/naming-module-groups.example.js[/]
```js
// test/examples/groups/naming-module-groups.example.js
export * as NamingModuleGroups from './grouped-module.example.js'
```

#### You can combine multiple test groups using `export *`

[//js]: ./test/examples/groups/grouped-module.example.js[/]
```js
// test/examples/groups/grouped-module.example.js
export * from '../classes/classGroup.example.js'
export * from '../objects/objectGroup.example.js'
```

#### You can name individual test groups using their exported name (or a title)

For naming test groups with titles see
  - [Naming class test groups](./testing-with-classes/class-test-titles.md)
  - [Naming object test groups](./testing-with-objects/object-test-titles.md)

[//js]: ./test/examples/classes/classGroup.example.js[/]
```js
// test/examples/classes/classGroup.example.js
import assert from 'node:assert';

export class ClassGroupExample1 {

  ['test 1']() {
    const thisGroup = this.suiteContext.test.parent.title
    const thisParentGroup = this.suiteContext.test.parent.parent.title

    assert.equal(thisGroup, "ClassGroupExample1");
    assert.equal(thisParentGroup, "NamingModuleGroups");
  }

}
```

[//js]: ./test/examples/objects/objectGroup.example.js[/]
```js
// test/examples/objects/objectGroup.example.js
import * as assert from "node:assert";

export const ObjectGroupExample1 = {

  'test 1': function () {
    const thisGroup = this.test.parent.title
    const thisParentGroup = this.test.parent.parent.title

    assert.equal(thisGroup, "ObjectGroupExample1");
    assert.equal(thisParentGroup, "NamingModuleGroups");
  },

}
```