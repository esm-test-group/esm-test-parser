# Using Only with pure function tests

> **NOTE**
Because exported functions are sorted by name in the 
parent import module we can only use `Only: true` or `Only: 1`.

#### Run a single function test

[//js]: ./test/examples/functions/functionOnly.example.js[/]
```js
import * as assert from "assert";
import { only } from 'esm-test-parser';

let testsRun = 0;

test1[only] = true;
export function test1() {
  testsRun++;
}

test2.title = "This test will not run";
export function test2() { throw new Error(this.test.title) }

export function afterAll() {
  assert.equal(testsRun, 1);
}
```
