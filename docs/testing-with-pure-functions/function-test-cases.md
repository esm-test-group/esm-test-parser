# Function Test Cases

[//js]: ./test/examples/functions/functionTestCases.example.js[/]
```js
import * as assert from "assert";
import { test } from 'esm-test-parser';

test1[test.title] = "test with cases";
test1[test.cases] = [
  [Math.PI, 3],
  [Math.PI * 2, 6]
]
export function test1(testRadians, expectedFloor) {
  const actual = Math.floor(testRadians);
  assert.equal(actual, expectedFloor);
}
```
