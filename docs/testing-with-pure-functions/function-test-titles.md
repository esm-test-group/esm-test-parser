# Function Test Titles

[//js]: ./test/examples/functions/functionTestTitles.example.js[/]
```js
import * as assert from "assert";
import { test } from 'esm-test-parser';

testWithTitle[test.title] = "test with title";
export function testWithTitle() {
  assert.equal(this.test.title, "test with title");
}

export function testNoTitle() {
  assert.equal(this.test.title, "testNoTitle");
}
```
