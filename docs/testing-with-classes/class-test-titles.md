# Class Test Titles

> **NOTE** @testTitle() decorator won't work unless decorators are enabled.
This parser currently supports typescript or babel decorators.

#### Using export names as titles for test groups

[//js]: ./test/examples/classes/titles/classExportTestGroupTitles.example.js[/]
```js
import * as assert from 'assert';
import { testTitle } from 'esm-test-parser';

/**
 * Hierarchy output
 * 
 *  ClassTestTitlesExample
 *    ✔ property name title
 *    ✔ decorator title
 *    ✔ testWithNoTitle
 */
export class ClassTestTitlesExample {

  // custom test title
  // js property names must be unique
  ['property name title']() {
    assert.equal(this.suiteContext.test.title, 'property name title');
  }

  // decorator title (needs decorator support)
  @testTitle('decorator title')
  testWithDecoratorTitle() {
    assert.equal(this.suiteContext.test.title, 'decorator title');
  }

  testWithNoTitle() {
    assert.equal(this.suiteContext.test.title, 'testWithNoTitle');
  }

  afterAll() {
    assert.equal(this.suiteContext.test.parent.title, 'Test title examples');
  }

}
```

#### Using custom titles for test groups

[//js]: ./test/examples/classes/titles/classCustomTestGroupTitles.example.js[/]
```js
import * as assert from 'assert';
import { testTitle } from 'esm-test-parser';

/**
 * Hierarchy output
 * 
 *  Test title examples
 *    ✔ property name title
 *    ✔ decorator title
 *    ✔ testWithNoTitle
 */
@testTitle('Test title examples')
export class ClassTestTitlesExample {

  // custom test title
  // js property names must be unique
  ['property name title']() {
    assert.equal(this.suiteContext.test.title, 'property name title');
  }

  // decorator title (needs decorator support)
  @testTitle('decorator title')
  testWithDecoratorTitle() {
    assert.equal(this.suiteContext.test.title, 'decorator title');
  }

  testWithNoTitle() {
    assert.equal(this.suiteContext.test.title, 'testWithNoTitle');
  }

  afterAll() {
    assert.equal(this.suiteContext.test.parent.title, 'Test title examples');
  }

}
```