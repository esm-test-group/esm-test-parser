# Class Only Expressions

> **NOTE**
`@testOnly()` won't work unless decorators are enabled.
This parser currently supports typescript or babel decorators.
If you need `only` statements but you can't use decorators 
then you can use [Object Only Expressions](../testing-with-objects/object-only-expressions.md) instead.

#### Run a single test using `@testOnly()`

[//js]: ./test/examples/classes/classOnly.example.js[/]
```js
import { testOnly } from 'esm-test-parser';

export class ClassOnlyExample {

  // runs this test only
  @testOnly()
  ['test 1']() {

  }

  ['This test will not run']() { throw new Error(this.suiteContext.test.title) }

}
```

#### Run all tests following the only expression by using `@testOnly('*')`

[//js]: ./test/examples/classes/classOnlyStar.example.js[/]
```js
import assert from 'node:assert';
import { testOnly } from 'esm-test-parser';

export class ClassOnlyStarExample {

  constructor () {
    this.testsRun = 0;
  }

  afterAll() {
    assert.equal(this.testsRun, 3);
  }

  ['This test will not run']() { throw new Error(this.suiteContext.test.title) }

  // runs all the following tests below this statement
  @testOnly('*')
  ['test 1']() {
    this.testsRun++;
  }

  ['test 2']() {
    this.testsRun++;
  }

  ['test 3']() {
    this.testsRun++;
  }

}
```

#### Run a specified number of tests from the only expression by using `@testOnly(#num)`

[//js]: ./test/examples/classes/classOnlyNumber.example.js[/]
```js
import assert from 'node:assert';
import { testOnly } from 'esm-test-parser';

export class ClassOnlyNumberExample {

  constructor () {
    this.testsRun = 0;
  }

  afterAll() {
    assert.equal(this.testsRun, 2);
  }

  ['This test will not run']() { throw new Error(this.suiteContext.test.title) }

  // runs the 2 tests following this statement
  @testOnly('2')
  ['test 1']() {
    this.testsRun++;
  }

  ['test 2']() {
    this.testsRun++;
  }

  ['This test will not run either']() { throw new Error(this.suiteContext.test.title) }

}
```
