# Class Test Cases

> **NOTE**
@testCase() won't work unless decorators are enabled.
This parser currently supports typescript or babel decorators.
If you need test cases but you can't use decorators 
then you can use [Object Test Cases](../testing-with-objects/object-test-cases.md) instead.

[//js]: ./test/examples/classes/classTestCases.example.js[/]
```js
import * as assert from 'assert';
import { testCase } from 'esm-test-parser';

export class ClassTestCasesExample {

  /**
   * Expected output
   * 
   * ClassTestCasesExample
   *   ✔ test (case 1)
   *   ✔ test (case 2)
   *   ✔ case index is 1
   *   ✔ case index is 2
   */

  // $i in the title will be replaced automatically
  // $i is 1 based. i.e. caseIndex of 0 will be presented as "1"

  // runs two test cases
  @testCase(Math.PI, 3)
  @testCase(Math.PI * 2, 6)
  ['test (case $i)'](testRadians, expectedFloor) {
    const actual = Math.floor(testRadians);
    assert.equal(actual, expectedFloor);
  }

  // a caseIndex argument is appended to the end of each test case argument list
  @testCase(0)
  @testCase(1)
  ['case index is $i'](expectedCaseIndex, caseIndex) {
    assert.equal(caseIndex, expectedCaseIndex);
    assert.equal(
      this.suiteContext.test.title,
      `case index is ${caseIndex + 1}`
    );
  }

}
```
