import Mocha from 'mocha';
import { JUnitReporter } from './junitReporter.js';

export class MultiReporter extends Mocha.reporters.Base {
  constructor (runner, options) {
    super(runner, options);
    this._specReporter = new Mocha.reporters.Spec(runner, options);
    this._junitReporter = new JUnitReporter(runner, options);
  }
}
