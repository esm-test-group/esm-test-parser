import fs from 'node:fs';
import path from 'node:path';
import Mocha from 'mocha';

const constants = Mocha.Runner.constants;
const EVENT_RUN_END = constants.EVENT_RUN_END;
const EVENT_SUITE_BEGIN = constants.EVENT_SUITE_BEGIN;
const EVENT_TEST_FAIL = constants.EVENT_TEST_FAIL;
const EVENT_TEST_PASS = constants.EVENT_TEST_PASS;
const EVENT_TEST_BEGIN = constants.EVENT_TEST_BEGIN;
const EVENT_TEST_END = constants.EVENT_TEST_END;
const Base = Mocha.reporters.Base;
const allSuiteName = "__all__";
const suiteMap = {};

export class JUnitReporter extends Base {

  static description = 'junit xml report generator';

  constructor (runner, mochaOptions) {
    super(runner, mochaOptions);
    const runnerOptions = mochaOptions.reporterOptions;
    const self = this;

    runner.on(EVENT_SUITE_BEGIN, (suite) => {
      const suiteFullTitle = getSuiteNameFromSuite(
        suite,
        runnerOptions.testSuiteNameDelimiter
      );

      suiteMap[suiteFullTitle] = {
        name: suiteFullTitle,
        failed: 0,
        testCount: 0,
        tests: {},
        time: 0
      }
    });

    runner.on(EVENT_TEST_BEGIN, (test) => {
      const suiteName = getSuiteNameFromTest(
        test,
        runnerOptions.testSuiteNameDelimiter
      );
      const suite = suiteMap[suiteName];
      suite.tests[test.id] = {
        name: test.title
      };
    });

    // mocha doesnt report the correct test.duration
    // runner.on(EVENT_TEST_END, (test) => {
    //   const suiteName = getSuiteNameFromTest(
    //     test,
    //     runnerOptions.testSuiteNameDelimiter
    //   );
    //   const suite = suiteMap[suiteName];
    //   suite.tests[test.id].time = test.duration;
    // });


    runner.on(EVENT_TEST_PASS, (test) => {
      const suiteName = getSuiteNameFromTest(
        test,
        runnerOptions.testSuiteNameDelimiter
      );

      const rootSuite = suiteMap[allSuiteName];
      rootSuite.testCount++;

      const suite = suiteMap[suiteName];
      suite.tests[test.id].failed = 0;
    });

    runner.on(EVENT_TEST_FAIL, (test) => {
      const suiteName = getSuiteNameFromTest(
        test,
        runnerOptions.testSuiteNameDelimiter
      );

      const rootSuite = suiteMap[allSuiteName];
      rootSuite.testCount++;
      rootSuite.failed++;

      const suite = suiteMap[suiteName];
      suite.failed++;

      const testLog = suite.tests[test.id];
      testLog.failed = 1;
      testLog.failure = {
        name: test.err.name,
        stack: test.err.stack
      };
    });

    runner.once(EVENT_RUN_END, () => {
      suiteMap[allSuiteName].time = self.stats.duration;

      const xmlBuilder = [
        `<?xml version="1.0" encoding="UTF-8" ?>`
      ];

      xmlBuilder.push.apply(xmlBuilder, appendTestSuitesXml(allSuiteName));

      writeFileSync(runnerOptions.jUnitFile, xmlBuilder.join('\n'));
    });
  }
}

function getSuiteNameFromSuite(suite, delimiter) {
  if (!suite.parent) return allSuiteName;
  return suite.titlePath().join(delimiter || ' ');
}

function getSuiteNameFromTest(test, delimiter) {
  if (!test.parent) return allSuiteName;
  return test.parent.titlePath().join(delimiter || ' ');
}

function appendTestSuitesXml(allSuiteName) {
  const allSuite = suiteMap[allSuiteName];
  const tag = "testsuites";
  const nameAttr = `name="Root"`;
  const testsAttr = `tests="${allSuite.testCount}"`;
  const failuresAttr = `failures="${allSuite.failed}"`;
  const timeAttr = `time="0"`

  const xmlBuilder = [];

  xmlBuilder.push(`<${tag} ${nameAttr} ${testsAttr} ${failuresAttr} ${timeAttr}>`)

  Object.keys(suiteMap)
    .filter(x => x !== allSuiteName)
    .forEach(suiteName => {
      xmlBuilder.push.apply(xmlBuilder, appendTestSuiteXml(suiteName));
    })

  xmlBuilder.push(`</${tag}>`)

  return xmlBuilder;
}

function appendTestSuiteXml(suiteName) {
  const suite = suiteMap[suiteName];
  const testCount = Object.keys(suite.tests).length;
  const tag = "testsuite";
  const nameAttr = `name="${xmlEncode(suiteName)}"`;
  const testsAttr = `tests="${testCount}"`;
  const failuresAttr = `failures="${suite.failed}"`;
  const timeAttr = `time="0"`

  const xmlBuilder = [];

  if (testCount > 0) {
    xmlBuilder.push(`  <${tag} ${nameAttr} ${testsAttr} ${failuresAttr} ${timeAttr}>`)
    xmlBuilder.push.apply(xmlBuilder, appendTestCaseXml(suiteName, suite));
    xmlBuilder.push(`  </${tag}>`)
  }

  return xmlBuilder;
}

function appendTestCaseXml(suiteName, suite) {
  const xmlBuilder = [];

  const classNameAttr = `classname="${xmlEncode(suiteName)}"`;

  Object.keys(suite.tests)
    .forEach((testName, index) => {
      const test = suite.tests[testName];
      const tag = "testcase";
      const nameAttr = `name="${index + 1}: ${xmlEncode(test.name)}"`;
      const timeAttr = `time="${test.time * 0.001}"`
      xmlBuilder.push(`    <${tag} ${nameAttr} ${timeAttr} ${classNameAttr}>`)

      if (test.failed > 0) {
        xmlBuilder.push.apply(xmlBuilder, appendFailureXml(test));
      }

      xmlBuilder.push(`    </${tag}>`)
    });

  return xmlBuilder;
}

function appendFailureXml(test) {
  const xmlBuilder = [];

  xmlBuilder.push(
    `      <failure message="${xmlEncode(test.failure.name)}" type="ERROR">`
  )

  xmlBuilder.push(
    `        ${xmlEncode(test.failure.stack)}`
  )

  xmlBuilder.push(
    `      </failure>`
  )

  return xmlBuilder;
}

function xmlEncode(value) {
  // &lt;	<	less than
  // &gt;	>	greater than
  // &amp;	&	ampersand
  // &apos;	'	apostrophe
  // &quot;	"	quotation mark
  let encoded = value;
  encoded = encoded.replaceAll("<", "&lt;");
  encoded = encoded.replaceAll(">", "&gt;");
  encoded = encoded.replaceAll("&", "&amp;");
  encoded = encoded.replaceAll("'", "&apos;");
  encoded = encoded.replaceAll("\"", "&quot;");
  return encoded;
}

function writeFileSync(outputFilePath, content) {
  const outputPath = path.resolve(
    process.cwd(),
    outputFilePath
  );

  const dirPath = path.dirname(outputPath);
  if (fs.existsSync(dirPath) === false) {
    fs.mkdirSync(dirPath);
  }

  fs.writeFileSync(outputPath, content, 'utf8')
}