import Mocha from 'mocha';
import registerMochaUiEsm from 'mocha-ui-esm';
import SourceMaps from 'source-map-support';

import * as E2eTestModules from './e2e/all.tests.js';
import { MultiReporter } from './reporters/multiReporter.js';
import * as UnitTestModules from './unit/all.tests.js';

const isE2E = process.argv[2] === "TEST_E2E";

// register esm test intergration
registerMochaUiEsm();

// create the test runner
const runner = new Mocha({
  ui: "esm",
  reporter: MultiReporter,
  reporterOptions: {
    jUnitFile: isE2E ? "./junit/e2e-tests.xml" : "./junit/unit-tests.xml",
    testSuiteNameDelimiter: "."
  },
  color: true,
  timeout: 60000,
})

// register tests
if (isE2E) {
  runner.suite.emit("modules", E2eTestModules)
} else {
  runner.suite.emit("modules", UnitTestModules)
}

// register soucemap support
SourceMaps.install();

// execute the tests
runner.run(
  failures => {
    if (process) process.exit(failures)
  }
)