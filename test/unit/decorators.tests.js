import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { testCase, testOnly, testTitle } from '../../src/decorators/all.js';
import { keywords } from '../../src/parser/keywords.js';

export const DecoratorTests = {

  [test.title]: "Decorators",

  '$1 decorator marks class instance function with "$3"': [
    ["@testOnly()", testOnly(), keywords.only],
    ["@testTitle()", testTitle('test title'), keywords.title],
    ["@testCase()", testCase(), keywords.cases],
    function (testTitle, testDecoratorFn, testDecoratorKey) {
      const testFnName = "test1"

      const testClass = class {
        [testFnName]() { }
      };

      const actualProto = testClass.prototype;

      testDecoratorFn(actualProto, testFnName);

      const actualHasKey = Object.hasOwn(actualProto[testFnName], testDecoratorKey);
      assert.ok(actualHasKey)
      assert.ok(actualProto[testFnName][testDecoratorKey]);
    }
  ],

  '$1 marks target class definition with "$3"': [
    ["@testOnly()", testOnly(), keywords.only],
    ["@testTitle()", testTitle('test title'), keywords.title],
    function (testTitle, testDecoratorFn, testDecoratorKey) {
      const testClassDef = class { };
      testDecoratorFn(testClassDef);

      const actualHasKey = Object.hasOwn(testClassDef, testDecoratorKey);
      assert.ok(actualHasKey);
      assert.ok(testClassDef[testDecoratorKey]);
    }
  ],

  '@testCase() cannot be applied to a class definition': () => {
    const testClassDef = class { };
    const testDecoratorFn = testCase(1, 2, 3);

    testDecoratorFn(testClassDef);

    const actualHasKey = Object.hasOwn(testClassDef, keywords.cases);

    assert.ok(actualHasKey === false);
  },

  '@testOnly("$1") expression is supported': [
    true,
    1,
    2,
    '*',
    function (testExpression) {
      const testClassDef = class { };
      const testDecoratorFn = testOnly(testExpression);
      try {
        testDecoratorFn(testClassDef);
      } catch (error) {
        assert.ok(
          false,
          `@testOnly('${testExpression}') failed`
        );
      }
    }
  ],

  '@testOnly("$1") expression is not supported': [
    undefined,
    null,
    'not supported',
    '!',
    '-1',
    -1,
    function (testExpression) {
      const testClassDef = class { };
      const testDecoratorFn = testOnly(testExpression);
      try {
        testDecoratorFn(testClassDef);
        assert.ok(
          false,
          `testOnly('${testExpression}') should fail`
        );
      } catch (error) {
        const expectedMessage = `testOnly('${testExpression}') expression on '${testClassDef.name}' not supported`;
        assert.equal(error.message, expectedMessage)
      }
    }
  ]

}