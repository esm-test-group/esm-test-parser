import * as assert from 'assert';
import {
  notComplexObjectMessage,
  notInstanceMessage,
  notObjectKeyMessage,
  notTypeMessage
} from '@esm-test/guards';
import { test } from 'esm-test-parser';
import { TestParserFlags } from '../../../src/options/testParserFlags.js';
import { TestParserOptions } from '../../../src/options/testParserOptions.js';
import { SortByEnum } from '../../../src/parser/models/testCollection.js';

export const TestParserOptionsTests = {

  [test.title]: TestParserOptions.name,

  constructor: {
    'throws error when "options" arg is not a complex object': [
      undefined,
      null,
      [["array1", "array2"]],
      0,
      "",
      function (testUserOptions) {
        try {
          new TestParserOptions(testUserOptions);
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notComplexObjectMessage("options")
          );
        }
      }
    ],

    'throws error when "builtInFunctions" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testBuiltInFunctions) {
        try {
          new TestParserOptions({ builtInFunctions: testBuiltInFunctions })
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("builtInFunctions", Array)
          );
        }
      }
    ],

    'throws error when "classContextPropertyName" arg is "$1"': [
      undefined,
      null,
      0,
      {},
      function (testName) {
        try {
          new TestParserOptions({
            builtInFunctions: [],
            classContextPropertyName: testName
          });
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notTypeMessage("classContextPropertyName", "string")
          );
        }
      }
    ],

    'throws error when "testContextLookup" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testContextLookup) {
        try {
          new TestParserOptions({
            builtInFunctions: [],
            classContextPropertyName: "",
            testContextLookup
          });
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("testContextLookup", Function)
          );
        }
      }
    ],

    'throws error when "testCaseTitleTemplate" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testCaseTitleTemplate) {
        try {
          new TestParserOptions({
            builtInFunctions: [],
            classContextPropertyName: "",
            testContextLookup: () => true,
            testCaseTitleTemplate
          });
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("testCaseTitleTemplate", Function)
          );
        }
      }
    ],

    'throws error when "parserFlags" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testParserFlags) {
        try {
          new TestParserOptions({
            builtInFunctions: [],
            classContextPropertyName: "",
            testContextLookup: () => true,
            testCaseTitleTemplate: () => true,
            parserFlags: testParserFlags
          });
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("parserFlags", TestParserFlags)
          );
        }
      }
    ],

    'throws error when "sort" arg is "$1"': [
      undefined,
      null,
      0,
      function (testSort) {
        try {
          new TestParserOptions({
            builtInFunctions: [],
            classContextPropertyName: "",
            testContextLookup: () => true,
            testCaseTitleTemplate: () => true,
            parserFlags: new TestParserFlags({}),
            sort: testSort
          });
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notObjectKeyMessage("sort", SortByEnum)
          );
        }
      }
    ],

    'assigns instance with "options" arg': function () {
      const testBuiltInFunctions = ["testBuiltIn"];
      const testClassContextPropertyName = "testContext";
      const testContextLookup = () => true;
      const testCaseTitleTemplate = () => true;
      const testParserFlags = new TestParserFlags(
        {
          allowLiteralOnly: true,
          allowLiteralTitle: false
        }
      )

      const expectedUserOptions = {
        builtInFunctions: testBuiltInFunctions,
        classContextPropertyName: testClassContextPropertyName,
        testContextLookup,
        testCaseTitleTemplate,
        parserFlags: testParserFlags,
        sort: SortByEnum.none
      };

      const actual = new TestParserOptions(expectedUserOptions);
      assert.deepEqual(actual, expectedUserOptions);
    }

  }

}