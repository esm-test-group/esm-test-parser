import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { notComplexObjectMessage, notTypeMessage } from '@esm-test/guards';
import { TestParserFlags } from '../../../src/options/testParserFlags.js';

export const parserFlagsTests = {

  [test.title]: TestParserFlags.name,

  constructor: {

    'throws error when "parserFlags" arg is not a complex object': [
      undefined,
      null,
      [["array1", "array2"]],
      0,
      "",
      function (testUserFlags) {
        try {
          new TestParserFlags(testUserFlags);
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notComplexObjectMessage("parserFlags")
          );
        }
      }
    ],

    'throws error when "allowLiteralOnly" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testAllowLiteralOnly) {
        try {
          new TestParserFlags({ allowLiteralOnly: testAllowLiteralOnly })
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notTypeMessage("allowLiteralOnly", "boolean")
          );
        }
      }
    ],

    'throws error when "allowLiteralOnly" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testAllowLiteralTitle) {
        try {
          new TestParserFlags({ allowLiteralTitle: testAllowLiteralTitle })
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notTypeMessage("allowLiteralTitle", "boolean")
          );
        }
      }
    ],

    'throws error when "allowLiteralCases" arg is "$1"': [
      undefined,
      null,
      0,
      "",
      function (testAllowLiteralCases) {
        try {
          new TestParserFlags({ allowLiteralCases: testAllowLiteralCases })
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notTypeMessage("allowLiteralCases", "boolean")
          );
        }
      }
    ],

  }

}