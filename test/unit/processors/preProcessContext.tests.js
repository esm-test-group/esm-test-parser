import * as assert from 'assert';
import { notInstanceMessage } from '@esm-test/guards';
import { test } from 'esm-test-parser';
import { TestParserOptions } from '../../../src/options/testParserOptions.js';
import { TestCollection } from '../../../src/parser/models/testCollection.js';
import { PreProcessContext } from '../../../src/processors/models/preProcessContext.js';

export const PreProcessContextTests = {

  [test.title]: PreProcessContext.name,

  constructor: {

    'throws error when "tests" arg is not valid': [
      undefined,
      null,
      0,
      "",
      function (testArray) {
        try {
          new PreProcessContext(testArray)
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("tests", TestCollection)
          );
        }
      }
    ],

    'throws error when "options" arg is $1': [
      undefined,
      null,
      0,
      "",
      function (testOptions) {
        try {
          new PreProcessContext(new TestCollection(), testOptions);
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("options", TestParserOptions)
          );
        }
      }
    ],

  },

  isBuiltInFunction: {

    beforeEach: function () {
      this.contextUnderTest = new PreProcessContext(
        new TestCollection(),
        new TestParserOptions({
          builtInFunctions: ["beforeEach"]
        })
      );
    },

    'returns true when built in name exists': function () {
      const actual = this.contextUnderTest.isBuiltInFunction("beforeEach");
      assert.equal(actual, true);
    },

    'returns false when builtin name does not exist': function () {
      const actual = this.contextUnderTest.isBuiltInFunction("notBuiltIn");
      assert.equal(actual, false);
    },

  }

}