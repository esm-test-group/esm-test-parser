import * as assert from 'assert';
import { notInstanceMessage, nullMessage, undefinedMessage } from '@esm-test/guards';
import { test } from 'esm-test-parser';
import { TestParserOptions } from '../../../src/options/testParserOptions.js';
import { TestCollection } from '../../../src/parser/models/testCollection.js';
import { PostProcessContext } from '../../../src/processors/models/postProcessContext.js';
import { createTestCaseFn } from '../../../src/processors/post/postProcessUtils.js';

export const PostProcessContextTests = {

  [test.title]: PostProcessContext.name,

  constructor: {

    'throws error when moduleToParse arg is "$1"': [
      [undefined, undefinedMessage("moduleToParse")],
      [null, nullMessage("moduleToParse")],
      [0, notInstanceMessage("moduleToParse", Object)],
      function (testModuleToParse, expectedMessage) {
        try {
          new PostProcessContext(testModuleToParse)
          assert.ok(false);
        } catch (e) {
          assert.equal(e.message, expectedMessage);
        }
      }
    ],

    'throws error when "options" arg is $1': [
      undefined,
      null,
      0,
      function (testOptions) {
        try {
          new PostProcessContext({}, testOptions)
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("options", TestParserOptions)
          );
        }
      }
    ],

    'throws error when "preTests" arg is $1': [
      undefined,
      null,
      0,
      function (testPreTests) {
        try {
          new PostProcessContext({}, new TestParserOptions({}), testPreTests)
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("preTests", TestCollection)
          );
        }
      }
    ],

    'throws error when "postTests" arg is $1': [
      undefined,
      null,
      0,
      function (testPostTests) {
        try {
          new PostProcessContext(
            {},
            new TestParserOptions({}),
            new TestCollection(),
            testPostTests
          );
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notInstanceMessage("postTests", TestCollection)
          );
        }
      }
    ],
  },

  createTestCaseFn: {

    beforeEach: function () {
      this.testGroupName = "testGroup";
      this.testContext = "context";
      this.testContextMap = {
        [this.testGroupName]: this.testContext
      };
      this.testContextLookup = key => this.testContextMap[key];
    },

    'returns test case arguments': function () {
      const testCases = [1, 2, 3];
      const testFn = function (...actualArgs) {
        return { actualThis: this, actualArgs };
      }

      const actual = createTestCaseFn(
        this.testGroupName,
        this.testContextLookup,
        testFn,
        testCases
      );

      const { actualThis, actualArgs } = actual();

      assert.deepEqual(actualThis, this.testContext);
      assert.equal(actualArgs.length, testCases.length);
      assert.deepEqual(actualArgs, testCases);
    },

    'returns test case arguments with done arg': function () {
      const testCases = [1];
      const testDoneArg = 1;

      const testFn = function (arg1, actualDone) {
        return {
          actualThis: this,
          actualArgs: [arg1],
          actualDone
        };
      }

      const actual = createTestCaseFn(
        this.testGroupName,
        this.testContextLookup,
        testFn,
        testCases
      );

      const { actualThis, actualArgs, actualDone } = actual(testDoneArg);

      assert.deepEqual(actualThis, this.testContext);
      assert.equal(actualArgs.length, testCases.length);
      assert.deepEqual(actualArgs, testCases);
      assert.equal(actualDone, testDoneArg);
    },

  }

}