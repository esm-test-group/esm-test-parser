import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { TestType } from '../../../../src/parser/models/testType.js';

export const TestTypeTests = {

  [test.title]: TestType.name,

  is: {
    'truth tests': [
      // expect true
      [TestType.HAS_ONLY, TestType.HAS_ONLY, true],
      [TestType.CLASS_INST_FUNCTION, TestType.CLASS_INST_FUNCTION, true],
      // expect false
      [TestType.FUNCTION_WITH_CASES, TestType.HAS_ONLY, false],
      [TestType.CLASS_INST_FUNCTION, TestType.FUNCTION, false],
      (testType, testFlag, expected) => {
        const actualInstance = new TestType(testType);
        const actual = actualInstance.is(testFlag);
        assert.ok(
          actual === expected,
          `${TestType.name}.is failed.`
          + `\r\ntestType was ${testType}`
          + `\r\ntestFlag was ${testFlag}`
          + `\r\nactualInstance.value was ${actualInstance.value}`
        );

      }
    ],
  },

  isEither: {
    'truth tests': [
      // expect true
      [TestType.CLASS_INST_FUNCTION, [TestType.CLASS_INST_FUNCTION], true],
      [TestType.CLASS_INST_FUNCTION, [TestType.HAS_CASES, TestType.CLASS_INST_FUNCTION], true],
      // expect false
      [TestType.CLASS_INST_FUNCTION, [TestType.FUNCTION], false],
      [TestType.CLASS_INST_FUNCTION, [TestType.HAS_ONLY, TestType.CLASS_INST], false],

      (testType, testFlags, expected) => {
        const actualInstance = new TestType(testType);
        const actual = actualInstance.isEither(...testFlags);
        assert.ok(
          actual === expected,
          `${TestType.name}.isEither failed.`
          + `\r\ntestType was ${testType}`
          + `\r\ntestFlag was ${testFlags}`
          + `\r\nactualInstance.value was ${actualInstance.value}`
        );
      }
    ],
  },

  has: {
    'truth tests': [
      // expect true
      [TestType.HAS_ONLY, TestType.HAS_ONLY, true],
      [TestType.HAS_CASES, TestType.HAS_CASES, true],
      [TestType.CLASS_INST_FUNCTION, TestType.CLASS_INST_FUNCTION, true],
      [TestType.CLASS_INST_FUNCTION_WITH_CASES, TestType.HAS_CASES, true],
      // expect false
      [TestType.HAS_ONLY, TestType.HAS_TITLE, false],
      [TestType.HAS_CASES, TestType.CLASS_DEF, false],
      [TestType.CLASS_INST_FUNCTION, TestType.BUILTIN_FUNCTION, false],
      [TestType.CLASS_INST_FUNCTION_WITH_CASES, TestType.EXPORT_DEFAULT, false],
      (testType, testFlag, expected) => {
        const actualInstance = new TestType(testType);
        const actual = actualInstance.has(testFlag);
        assert.ok(
          actual === expected,
          `${TestType.name}.has failed.`
          + `\r\ntestType was ${testType}`
          + `\r\ntestFlag was ${testFlag}`
          + `\r\nactualInstance.value was ${actualInstance.value}`
        );
      }
    ],
  },

  hasEither: {
    'truth tests': [
      // expect true
      [TestType.CLASS_INST_FUNCTION, [TestType.FUNCTION], true],
      [TestType.CLASS_INST_FUNCTION, [TestType.HAS_ONLY, TestType.CLASS_INST], true],
      // expect false
      [TestType.CLASS_INST_FUNCTION, [TestType.HAS_ONLY], false],
      [TestType.CLASS_INST_FUNCTION, [TestType.HAS_ONLY, TestType.HAS_TITLE], false],
      (testType, testFlags, expected) => {
        const actualInstance = new TestType(testType);
        const actual = actualInstance.hasEither(...testFlags);
        assert.ok(
          actual === expected,
          `${TestType.name}.hasEither failed.`
          + `\r\ntestType was ${testType}`
          + `\r\ntestFlag was ${testFlags}`
          + `\r\nactualInstance.value was ${actualInstance.value}`
        );
      }
    ],
  },

  toString: {

    'matches literal type name': [
      [TestType.HAS_ONLY, 'HAS_ONLY'],
      [TestType.HAS_CASES, 'HAS_CASES'],
      [TestType.CLASS_INST_FUNCTION, 'CLASS_INST_FUNCTION'],
      [TestType.OBJECT_WITH_ONLY, 'OBJECT_WITH_ONLY'],
      (testType, expected) => {
        const actualInstance = new TestType(testType);
        assert.equal(actualInstance.toString(), expected);
      }
    ]

  }

}