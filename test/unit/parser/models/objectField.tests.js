import * as assert from 'assert';
import { test } from 'esm-test-parser';
import {
  notTypeMessage,
  nullMessage,
  undefinedMessage
} from '@esm-test/guards';
import { keywords } from '../../../../src/parser/keywords.js';
import { ObjectField } from '../../../../src/parser/models/objectField.js';

export const ObjectFieldTests = {

  [test.title]: ObjectField.name,

  constructor: {

    'throws error when "groupName" arg is not a string': [
      undefined,
      null,
      [["array1", "array2"]],
      0,
      function (testGroupName) {
        try {
          new ObjectField(testGroupName);
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notTypeMessage("groupName", "string")
          );
        }
      }
    ],

    'throws error when "name" arg is not a string': [
      undefined,
      null,
      [["array1", "array2"]],
      0,
      function (testName) {
        try {
          new ObjectField("groupName", testName);
          assert.ok(false);
        } catch (e) {
          assert.equal(
            e.message,
            notTypeMessage("name", "string")
          );
        }
      }
    ],

    'throws error when "value" arg is "$1"': [
      [undefined, undefinedMessage("value")],
      [null, nullMessage("value")],
      function (testValue, expectedMessage) {
        try {
          new ObjectField("groupName", "name", testValue);
          assert.ok(false);
        } catch (e) {
          assert.equal(e.message, expectedMessage);
        }
      }
    ],

  },

  isDefaultExport: {

    'returns true when name === "default"': function () {
      const testField = new ObjectField("groupName", "default", {});
      assert.equal(testField.isDefaultExport, true);
    },

    'returns false when name !== "default"': function () {
      const testField = new ObjectField("groupName", "test1", {});
      assert.equal(testField.isDefaultExport, false);
    }

  },

  isFunction: {

    'returns true when value is a function': function () {
      const testField = new ObjectField("groupName", "function test", () => true);
      assert.equal(testField.isFunction, true);
    },

    'returns true when value is not a function': function () {
      const testField = new ObjectField("groupName", "not function test", {});
      assert.equal(testField.isFunction, false);
    },

  },

  isFunctionWithCases: {

    'returns true when the value contains test cases': [
      [[[1], () => true]],
      [[[1, 2], () => true]],
      function (testFunctionCases) {
        const testField = new ObjectField(
          "groupName",
          "function test",
          testFunctionCases
        );
        assert.equal(testField.isFunctionWithCases, true);
      }
    ],

    'returns false when the value does not contain test cases': [
      () => true,
      {},
      function (testFunctionCases) {
        const testField = new ObjectField(
          "groupName",
          "not function with cases test",
          testFunctionCases
        );
        assert.equal(testField.isFunctionWithCases, false);
      }
    ],

  },

  hasCasesDecorator: {

    'returns true when value contains a cases decorator': function () {
      const testField = new ObjectField(
        "groupName",
        "name",
        {
          [keywords.cases]: [1, 2, 3]
        }
      );

      assert.equal(testField.hasCasesDecorator, true);
    },

    'returns false when value does not contain a cases decorator': [
      {},
      { [keywords.cases]: '' },
      { [keywords.cases]: 0 },
      function (testValue) {
        const testField = new ObjectField(
          "groupName",
          "name",
          testValue
        );

        assert.equal(testField.hasCasesDecorator, false);
      }
    ],
  },

}