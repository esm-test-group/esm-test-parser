import {
  notInstanceMessage,
  notTypeMessage,
  nullMessage,
  undefinedMessage
} from '@esm-test/guards';
import { test } from 'esm-test-parser';
import { equal, ok } from 'node:assert';
import { Test, TestType } from '../../../../src/parser/models/all.js';

export const TestModelTests = {

  [test.title]: Test.name,

  constructor: {

    'throws error when "groupName" is not a type of string': [
      undefined,
      null,
      function (testGroupName) {
        try {
          new Test(testGroupName)
          ok(false);
        } catch (e) {
          equal(
            e.message,
            notTypeMessage("groupName", "string")
          )
        }
      }
    ],

    'throws error when "name" is not a type of string': [
      undefined,
      null,
      function (testName) {
        try {
          new Test("testGroupName", testName)
          ok(false);
        } catch (e) {
          equal(
            e.message,
            notTypeMessage("name", "string")
          )
        }
      }
    ],

    'throws error when "value" arg is not valid': [
      [undefined, undefinedMessage("value")],
      [null, nullMessage("value")],
      function (testValue, expectedMessage) {
        try {
          new Test("testGroupName", "testName", testValue)
          ok(false);
        } catch (e) {
          equal(e.message, expectedMessage);
        }
      }
    ],

    'throws error when "type" arg is not valid': [
      undefined,
      null,
      "",
      function (testType) {
        try {
          new Test("testGroupName", "testName", {}, testType)
          ok(false);
        } catch (e) {
          equal(
            e.message,
            notInstanceMessage("type", TestType)
          );
        }
      }
    ],

    'throws error when "testCases" arg is not valid': [
      undefined,
      null,
      "",
      function (testCases) {
        try {
          new Test(
            "testGroupName",
            "testName",
            {},
            new TestType(1),
            testCases
          );
          ok(false);
        } catch (e) {
          equal(
            e.message,
            notInstanceMessage("testCases", Array)
          );
        }
      }
    ],

    'throws error when "onlyExpression" arg is not valid': [
      [undefined, undefinedMessage("onlyExpression")],
      [null, nullMessage("onlyExpression")],
      function (testOnlyExpression, expectedMessage) {
        try {
          new Test(
            "testGroupName",
            "testName",
            {},
            new TestType(1),
            [],
            testOnlyExpression
          );
          ok(false);
        } catch (e) {
          equal(e.message, expectedMessage);
        }
      }
    ],

  },

  key: {

    'group name and test name are combined ': () => {
      const testGroupName = 'testGroupName';
      const testName = 'testName';
      const expectedKey = `${testGroupName}_${testName}`;

      // test
      const testModel = new Test(
        testGroupName,
        testName,
        {},
        new TestType(1),
        [],
        ''
      );

      // assert
      equal(testModel.key, expectedKey)
    }

  }

}