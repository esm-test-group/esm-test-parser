import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { TestCollection, TestType } from '../../../../../src/parser/models/all.js';
import { filterFixture } from './testCollection.fixtures.js';

export const filterHasEitherTests = {

  [test.title]: TestCollection.prototype.filterHasEither.name,

  'should match filtered results': [
    [[TestType.HAS_ONLY, undefined, null], 2],
    [[TestType.HAS_ONLY, TestType.CLASS_DEF], 2],
    [[TestType.HAS_ONLY, TestType.FUNCTION], 7],
    [[TestType.FUNCTION], 5],
    (testTypeValues, expectedCount) => {
      const actual = filterFixture.filterHasEither(...testTypeValues);

      assert.equal(
        actual.length,
        expectedCount,
        "TestCollection count did not match"
      );

      actual.forEach(test => {
        assert.ok(
          test.type.hasEither(...testTypeValues),
          "TestCollection filter results did not match"
        );
      });
    }
  ],

}