import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { TestCollection, TestType } from '../../../../../src/parser/models/all.js';
import { filterFixture } from './testCollection.fixtures.js';

export const filterIsEitherTests = {

  [test.title]: TestCollection.prototype.filterIsEither.name,

  'should match filtered results': [
    [[TestType.HAS_ONLY, TestType.FUNCTION], 4],
    [[TestType.FUNCTION, TestType.FUNCTION_WITH_CASES], 5],
    [[TestType.HAS_ONLY, TestType.FUNCTION_WITH_CASES], 5],
    (testTypeValues, expectedCount) => {
      const actual = filterFixture.filterIsEither(...testTypeValues);

      assert.equal(
        actual.length,
        expectedCount,
        "TestCollection count did not match"
      );

      actual.forEach(test => {
        assert.ok(
          test.type.isEither(...testTypeValues),
          "TestCollection filter results did not match"
        );
      });
    }
  ],

  'should not match any results': [
    [[undefined, null]],
    [[TestType.CLASS_INST, TestType.CLASS_INST_BUILTIN_FUNCTION]],
    [[TestType.CLASS_DEF_EXPORT_DEFAULT, TestType.EXPORT_DEFAULT]],
    (testTypeValues) => {
      const actual = filterFixture.filterIsEither(...testTypeValues);
      assert.equal(actual.length, 0, "TestCollection count did not match");
    }
  ],

}