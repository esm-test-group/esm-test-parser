import { Test, TestCollection, TestType } from "../../../../../src/parser/models/all.js";

export const filterFixture = new TestCollection([
  new Test("group1", "group1 -> test1", {}, new TestType(TestType.FUNCTION), [], ""),
  new Test("group1", "group1 -> test2", {}, new TestType(TestType.FUNCTION_WITH_CASES), [], ""),
  new Test("group1", "group1 -> test3", {}, new TestType(TestType.HAS_ONLY), [], ""),
  new Test("group1", "group1 -> test4", {}, new TestType(TestType.FUNCTION), [], ""),
  new Test("group2", "group2 -> test1", {}, new TestType(TestType.FUNCTION_WITH_CASES), [], ""),
  new Test("group2", "group2 -> test2", {}, new TestType(TestType.HAS_ONLY), [], ""),
  new Test("group2", "group2 -> test3", {}, new TestType(TestType.FUNCTION_WITH_CASES), [], ""),
]);

export const sortFixture = new TestCollection([
  new Test("group1", "group1", {}, new TestType(TestType.MODULE), [], ""),
  new Test("group1", "group1 -> test1", {}, new TestType(TestType.FUNCTION), [], ""),
  new Test("group1", "group1 -> test2", {}, new TestType(TestType.FUNCTION_WITH_CASES), [], ""),
  new Test("group3", "group3", {}, new TestType(TestType.MODULE), [], ""),
  new Test("group3", "group3 -> test2", {}, new TestType(TestType.HAS_ONLY), [], ""),
  new Test("group3", "group3 -> test1", {}, new TestType(TestType.FUNCTION_WITH_CASES), [], ""),
  new Test("group2", "group2", {}, new TestType(TestType.MODULE), [], ""),
  new Test("group2", "group2 -> test1", {}, new TestType(TestType.FUNCTION), [], ""),
  new Test("group2", "group2 -> test2", {}, new TestType(TestType.HAS_ONLY), [], ""),
  new Test("group4", "group4", {}, new TestType(TestType.MODULE), [], ""),
  new Test("group4", "xyz", {}, new TestType(TestType.HAS_ONLY), [], ""),
  new Test("group4", "abc", {}, new TestType(TestType.HAS_ONLY), [], ""),
]);
