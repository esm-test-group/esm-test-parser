import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { SortByMethod, TestCollection } from '../../../../../src/parser/models/all.js';
import { sortFixture } from './testCollection.fixtures.js';

export const sortByTests = {

  [test.title]: TestCollection.prototype.sortBy.name,

  'should sort by groups only': () => {
    const expectedGroupSort = [
      { groupName: "group1", name: 'group1' },
      { groupName: "group2", name: 'group2' },
      { groupName: "group3", name: 'group3' },
      { groupName: "group4", name: 'group4' },
      { groupName: "group1", name: 'group1 -> test1' },
      { groupName: "group1", name: 'group1 -> test2' },
      { groupName: "group3", name: 'group3 -> test2' },
      { groupName: "group3", name: 'group3 -> test1' },
      { groupName: "group2", name: 'group2 -> test1' },
      { groupName: "group2", name: 'group2 -> test2' },
      { groupName: "group4", name: 'xyz' },
      { groupName: "group4", name: 'abc' },
    ];

    const actual = sortFixture.sortBy(SortByMethod.groupsOnly)

    assert.equal(actual.length, expectedGroupSort.length);

    expectedGroupSort.forEach((expected, index) => {
      assert.equal(expected.groupName, actual[index].groupName)
      assert.equal(expected.name, actual[index].name)
    })
  },

  'should sort by tests only': () => {
    const expectedTitleSort = [
      { groupName: "group1", name: 'group1' },
      { groupName: "group3", name: 'group3' },
      { groupName: "group2", name: 'group2' },
      { groupName: "group4", name: 'group4' },
      { groupName: "group4", name: 'abc' },
      { groupName: "group1", name: 'group1 -> test1' },
      { groupName: "group1", name: 'group1 -> test2' },
      { groupName: "group2", name: 'group2 -> test1' },
      { groupName: "group2", name: 'group2 -> test2' },
      { groupName: "group3", name: 'group3 -> test1' },
      { groupName: "group3", name: 'group3 -> test2' },
      { groupName: "group4", name: 'xyz' },
    ];

    const actual = sortFixture.sortBy(SortByMethod.testsOnly)

    assert.equal(actual.length, expectedTitleSort.length);

    expectedTitleSort.forEach((expected, index) => {
      assert.equal(expected.groupName, actual[index].groupName)
      assert.equal(expected.name, actual[index].name)
    })
  },

  'should sort by all': () => {
    const expectedTitleAndGroupSort = [
      { groupName: "group1", name: 'group1' },
      { groupName: "group2", name: 'group2' },
      { groupName: "group3", name: 'group3' },
      { groupName: "group4", name: 'group4' },
      { groupName: "group4", name: 'abc' },
      { groupName: "group1", name: 'group1 -> test1' },
      { groupName: "group1", name: 'group1 -> test2' },
      { groupName: "group2", name: 'group2 -> test1' },
      { groupName: "group2", name: 'group2 -> test2' },
      { groupName: "group3", name: 'group3 -> test1' },
      { groupName: "group3", name: 'group3 -> test2' },
      { groupName: "group4", name: 'xyz' },
    ];

    const actual = sortFixture.sortBy(SortByMethod.all)

    assert.equal(actual.length, expectedTitleAndGroupSort.length);

    expectedTitleAndGroupSort.forEach((expected, index) => {
      assert.equal(expected.groupName, actual[index].groupName)
      assert.equal(expected.name, actual[index].name)
    })
  }

}