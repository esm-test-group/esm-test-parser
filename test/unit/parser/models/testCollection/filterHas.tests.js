import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { TestCollection, TestType } from '../../../../../src/parser/models/all.js';
import { filterFixture } from './testCollection.fixtures.js';

export const filterHasTests = {

  [test.title]: TestCollection.prototype.filterHas.name,

  'should match filtered results': [
    [TestType.FUNCTION, 5],
    (testTypeValue, expectedCount) => {
      const actual = filterFixture.filterHas(testTypeValue);

      assert.equal(
        actual.length,
        expectedCount,
        "TestCollection count did not match"
      );

      actual.forEach(test => {
        assert.ok(
          test.type.has(testTypeValue),
          "TestCollection filter results did not match"
        );
      });
    }
  ],

  'should not match any results': [
    undefined,
    null,
    TestType.CLASS_INST,
    TestType.CLASS_INST_BUILTIN_FUNCTION,
    (testTypeValue) => {
      const actual = filterFixture.filterHas(testTypeValue);
      assert.equal(actual.length, 0, "TestCollection count did not match");
    }
  ],

}