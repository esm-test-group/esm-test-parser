import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { TestCollection, TestType } from '../../../../../src/parser/models/all.js';
import { filterFixture } from './testCollection.fixtures.js';

export const filterIsTests = {

  [test.title]: TestCollection.prototype.filterIs.name,

  'should match filtered results': [
    [TestType.HAS_ONLY, 2],
    [TestType.FUNCTION, 2],
    [TestType.FUNCTION_WITH_CASES, 3],
    (testTypeValue, expectedCount) => {
      const actual = filterFixture.filterIs(testTypeValue);

      assert.equal(
        actual.length,
        expectedCount,
        "TestCollection count did not match"
      );

      actual.forEach(test => {
        assert.equal(
          test.type.value,
          testTypeValue,
          "TestCollection filter results did not match"
        );
      });
    }
  ],

  'should not match any results': [
    undefined,
    null,
    TestType.CLASS_INST,
    TestType.CLASS_INST_BUILTIN_FUNCTION,
    (testTypeValue) => {
      const actual = filterFixture.filterIs(testTypeValue);

      assert.equal(actual.length, 0, "TestCollection count did not match");

      actual.forEach(test => {
        assert.notEqual(
          test.type.value,
          testTypeValue,
          "TestCollection filter results did not match"
        );
      });
    }
  ],

}