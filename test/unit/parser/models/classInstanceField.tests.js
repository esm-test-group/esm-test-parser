import * as assert from 'assert';
import { test } from 'esm-test-parser';
import {
  nullMessage,
  undefinedMessage
} from '@esm-test/guards';
import { ClassInstanceField } from '../../../../src/parser/models/classInstanceField.js';

export const ClassInstanceFieldTests = {

  [test.title]: ClassInstanceField.name,

  constructor: {

    'throws error when "classInstance" arg is "$1"': [
      [undefined, undefinedMessage("classInstance")],
      [null, nullMessage("classInstance")],
      function (testClassInstance, expectedMessage) {
        try {
          new ClassInstanceField("groupName", "name", {}, testClassInstance);
          assert.ok(false);
        } catch (e) {
          assert.equal(e.message, expectedMessage);
        }
      }
    ],

  },

}