import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { parseObjectFields } from '../../../src/parser/parseObjectFields.js';

export const parseObjectFieldsTests = {

  [test.title]: parseObjectFields.name,

  'returns fields from objects': () => {
    const testGroupName = "testGroup";
    const testModule = {
      test1: () => true,
      only: true,
      title: ""
    }

    const expectedFieldKeys = Object.keys(testModule);

    const actualFields = parseObjectFields(testGroupName, testModule)

    assert.equal(actualFields.length, expectedFieldKeys.length);

    expectedFieldKeys.forEach((key, index) => {
      const actualField = actualFields[index];
      assert.equal(actualField.groupName, testGroupName);
      assert.equal(actualField.name, key);
      assert.equal(actualField.value, testModule[key]);
    })

  },

}