import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { parserClassInstFields } from '../../../src/parser/parseClassInstFields.js';

export const parseClassInstFieldsTests = {

  [test.title]: parserClassInstFields.name,

  'returns fields from class instance': () => {
    const testGroupName = "testGroup";
    const testClassInst = new class {
      constructor () {
        this.test = 123;
      }

      test1() { }
    };

    const testClassProto = Reflect.getPrototypeOf(testClassInst);

    const expectedFieldKeys = Reflect.ownKeys(testClassProto)
      .filter(x => x !== "constructor");

    const actualFields = parserClassInstFields(testGroupName, testClassInst);

    assert.equal(actualFields.length, expectedFieldKeys.length);

    expectedFieldKeys.forEach((key, index) => {
      const actualField = actualFields[index];
      assert.equal(actualField.groupName, testGroupName);
      assert.equal(actualField.name, key);
      assert.equal(actualField.value, testClassProto[key]);
      assert.deepEqual(actualField.classInstance, testClassInst);
    });

  },

}