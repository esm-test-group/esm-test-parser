import * as assert from 'assert';
import { TestParserFlags } from '../../../../src/options/testParserFlags.js';
import { keywords } from '../../../../src/parser/keywords.js';
import {
  getFunctionTestCases,
  getPureFunctionCases,
  hasPureFunctionCases,
  simpleReplace
} from '../../../../src/parser/utils/testCaseUtils.js';

export const TestCaseUtils = {

  simpleReplace: {

    'returns title with test case replacements': [
      ["title without args", [1, 2], "title without args"],
      ["$i: title with case index", [1, 2], "2: title with case index"],
      ["title with two args - $1 $2", [1, 2], "title with two args - 1 2"],
      ["title with three args - $1 $2 $3", [3, 4, 5], "title with three args - 3 4 5"],
      ["titles with arrays - $1 $2", [[1, 2, 3], [4, 5, 6]], "titles with arrays - 1,2,3 4,5,6"],
      ["titles with objects - $1", [{ key1: 123 }], "titles with objects - [object Object]"],
      ["titles with undefined - $1", [undefined], "titles with undefined - undefined"],
      ["titles with null - $1", [null], "titles with null - null"],
      ["title with number args - $1", [1], "title with number args - 1"],
      ["title with string args - $1", ["test"], "title with string args - test"],
      function (testTitle, testCaseArgs, expected, caseIndex) {
        const actual = simpleReplace(
          testTitle,
          testCaseArgs,
          caseIndex
        );

        assert.equal(actual, expected);

        this.test.title = actual;
      }
    ],

  },

  hasPureFunctionCases: {

    beforeEach: function () {
      this.testFn = () => true;
    },

    'returns true when value has keywords.cases key': function () {
      this.testFn[keywords.cases] = [];
      const actual = hasPureFunctionCases(this.testFn);
      assert.ok(actual);
    },

    'returns true when value has literal "testCases" and enabled': function () {
      this.testFn[keywords.cases] = [];
      const actual = hasPureFunctionCases(
        this.testFn,
        { parserFlags: new TestParserFlags({ allowLiteralCases: true }) }
      );
      assert.ok(actual);
    },

    'returns false when value has literal "testCases" and disabled': function () {
      this.testFn["testCases"] = [];
      const actual = hasPureFunctionCases(
        this.testFn,
        { parserFlags: new TestParserFlags({ allowLiteralCases: false }) }
      );
      assert.equal(actual, false);
    },

    'returns false when does not have keywords.cases or literal "testCases"': function () {
      const actual = hasPureFunctionCases(
        this.testFn,
        { parserFlags: new TestParserFlags({}) }
      );
      assert.equal(actual, false);
    },

  },

  getPureFunctionCases: {

    beforeEach: function () {
      this.testFn = () => true;
    },

    'returns an array of test case arrays': function () {
      const testCases = [
        [1, 2, 3],
        [4, 5, 6]
      ];

      this.testFn[keywords.cases] = testCases;

      const actual = getPureFunctionCases(this.testFn);

      assert.deepEqual(actual, testCases);
    },

    'returns an array of single test case arrays': function () {
      const testCases = [
        1,
        2,
        "test",
        { key1: 123 },
        undefined,
        null
      ];

      const expectedCases = testCases.map(x => [x]);

      this.testFn[keywords.cases] = testCases;

      const actual = getPureFunctionCases(this.testFn);

      assert.deepEqual(actual, expectedCases);
    },

  },

  getFunctionTestCases: {

    'returns an array of test case arrays': function () {
      const testCases = [
        [1, 2, 3],
        [4, 5, 6]
      ];

      const actual = getFunctionTestCases([
        ...testCases,
        () => true
      ]);

      assert.deepEqual(actual, testCases);
    },

    'returns an array of single test case arrays': function () {
      const testCases = [
        1,
        2,
        "test",
        { key1: 123 },
        undefined,
        null
      ];

      const expectedCases = testCases.map(x => [x]);

      const actual = getFunctionTestCases([
        ...testCases,
        () => true
      ]);

      assert.deepEqual(actual, expectedCases);
    },

  }

}