import * as assert from 'assert';
import { isModule } from '../../../../src/parser/utils/moduleUtils.js';
import { createModule } from '../../../e2e/utils.js';

export const ModuleUtils = {

  isModule: {

    'returns true for modules': () => {
      const testModule = new createModule({})
      const actual = isModule(testModule)
      assert.ok(actual);
    },

    'returns false for non modules': [
      {},
      class { },
      (testModule) => {
        const actual = isModule(testModule)
        assert.equal(actual, false);
      }
    ],

  },

}