import * as assert from 'assert';
import { TestParserFlags } from '../../../../src/options/testParserFlags.js';
import { keywords } from '../../../../src/parser/keywords.js';
import {
  getTitle,
  hasTitle,
  hasTitleDecorator
} from '../../../../src/parser/utils/testTitleUtils.js';

export const TestTitleUtils = {

  hasTitle: {

    'returns true when value has keywords.title key': () => {
      const actual = hasTitle({ [keywords.title]: true });
      assert.ok(actual);
    },

    'returns true when value has literal "title" and enabled': () => {
      const actual = hasTitle(
        { title: true },
        { parserFlags: new TestParserFlags({ allowLiteralTitle: true }) }
      );
      assert.ok(actual);
    },

    'returns false when value has literal "title" and disabled': () => {
      const actual = hasTitle(
        { title: true },
        { parserFlags: new TestParserFlags({ allowLiteralTitle: false }) }
      );
      assert.equal(actual, false);
    },

    'returns false when does not have keywords.title or literal "title"': () => {
      const actual = hasTitle(
        {},
        { parserFlags: new TestParserFlags({}) }
      );
      assert.equal(actual, false);
    },

  },

  getTitle: {

    'returns the title string when value has keywords.title key': () => {
      const testTitle = "test title";
      const actual = getTitle({ [keywords.title]: "test title" });
      assert.ok(actual, testTitle);
    },

    'returns the title string when value has literal "title" and enabled': () => {
      const testTitle = "test title";
      const actual = getTitle(
        { title: testTitle },
        { parserFlags: new TestParserFlags({ allowLiteralTitle: true }) }
      );
      assert.ok(actual, testTitle);
    },

    'returns false when value has literal "title" and disabled': () => {
      const actual = getTitle(
        { title: "test title" },
        { parserFlags: new TestParserFlags({ allowLiteralTitle: false }) }
      );
      assert.equal(actual, false);
    },

  },

  hasTitleDecorator: {

    'returns true when value contains a title decorator': function () {
      const testValue = {
        [keywords.title]: "test title"
      };

      const actual = hasTitleDecorator(testValue);

      assert.ok(actual);
    },

    'returns false when value does not contain a title decorator': function () {
      const actual = hasTitleDecorator({});
      assert.equal(actual, false);
    }

  },

}