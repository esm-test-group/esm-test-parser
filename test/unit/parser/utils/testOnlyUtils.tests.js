import * as assert from 'assert';
import { TestParserFlags } from '../../../../src/options/testParserFlags.js';
import { keywords } from '../../../../src/parser/keywords.js';
import {
  hasOnly,
  hasOnlyDecorator,
  isOnlyExpressionSupported,
  isOnlyField
} from '../../../../src/parser/utils/testOnlyUtils.js';

export const TestOnlyUtils = {

  isOnlyField: {

    'returns true when name matches keywords.only value': () => {
      const actual = isOnlyField(
        keywords.only,
        { parserFlags: new TestParserFlags({ allowLiteralOnly: false }) }
      );
      assert.ok(actual);
    },

    'returns true when name is literal "only" and enabled': () => {
      const actual = isOnlyField(
        "only",
        { parserFlags: new TestParserFlags({ allowLiteralOnly: true }) }
      );
      assert.ok(actual);
    },

    'returns false when name is literal "only" and disabled': () => {
      const actual = isOnlyField(
        "only",
        { parserFlags: new TestParserFlags({ allowLiteralOnly: false }) }
      );
      assert.equal(actual, false);
    },

  },

  hasOnly: {

    'returns true when value has keywords.only key': () => {
      const actual = hasOnly({ [keywords.only]: true });
      assert.ok(actual);
    },

    'returns true when value has literal "only" and enabled': () => {
      const actual = hasOnly(
        { only: true },
        { parserFlags: new TestParserFlags({ allowLiteralOnly: true }) }
      );
      assert.ok(actual);
    },

    'returns false when value has literal "only" and disabled': () => {
      const actual = hasOnly(
        { only: true },
        { parserFlags: new TestParserFlags({ allowLiteralOnly: false }) }
      );
      assert.equal(actual, false);
    },

    'returns false when does not have keywords.only or literal "only"': () => {
      const actual = hasOnly(
        {},
        { parserFlags: new TestParserFlags({}) }
      );
      assert.equal(actual, false);
    },

  },

  hasOnlyDecorator: {

    'returns true when value contains a supported only decorator - $1': [
      true,
      1,
      '*',
      function (testOnlyExpression) {
        const testValue = {
          [keywords.only]: testOnlyExpression
        };
        const actual = hasOnlyDecorator(testValue);
        assert.equal(actual, true);
      }
    ],

    '$i: returns false when value does not contain a supported only decorator': [
      {},
      { [keywords.only]: '' },
      { [keywords.only]: 0 },
      { [keywords.only]: -1 },
      function (testValue) {
        const actual = hasOnlyDecorator(testValue);
        assert.equal(actual, false);
      }
    ],

  },

  isOnlyExpressionSupported: {

    'returns true for $1 expression': [
      true,
      1,
      2,
      '*',
      (testExpression) => {
        const actual = isOnlyExpressionSupported(testExpression);
        assert.ok(actual);
      }
    ],

    'returns false for "$1" expression': [
      null,
      [[1, 2, 3]],
      'not supported',
      '!',
      '-1',
      -1,
      (testExpression) => {
        const actual = isOnlyExpressionSupported(testExpression);
        assert.equal(actual, false);
      }
    ],

  }
}