import * as assert from 'assert';
import { isClassDefinition, isClassInstance } from '../../../../src/parser/utils/classUtils.js';

export const ClassUtils = {

  isClassDefinition: {

    'returns true for class definitions': () => {
      const actual = isClassDefinition(class { })
      assert.ok(actual);
    },

    'returns false for non class definitions': () => {
      const actual = isClassDefinition(new Error())
      assert.equal(actual, false);
    },

  },

  isClassInstance: {

    'returns true for class instances': () => {
      const actual = isClassInstance(new class { })
      assert.ok(actual);
    },

    'returns false for non class instances': () => {
      const actual = isClassInstance(class { })
      assert.equal(actual, false);
    },

  },
}