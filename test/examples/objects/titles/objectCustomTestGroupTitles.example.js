import * as assert from 'assert';
import { test } from 'esm-test-parser';

/**
 * Hierarchy output
 * 
 * Test group title example 1
 *  ✔ test
 */
export const ObjectCustomTestGroupTitle = {

  // set a custom test group title
  [test.title]: "Test group title example 1",

  test: function () {
    assert.equal(this.test.parent.title, "Test group title example 1")
  }

}