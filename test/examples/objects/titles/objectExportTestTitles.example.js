import * as assert from 'assert';

/**
 * Hierarchy output
 * 
 * ObjectExportTestGroupTitle
 *  ✔ property name title
 *  ✔ testWithNoTitle
 */
export const ObjectExportTestGroupTitle = {

  // test titles are taken from the property names.
  // js property names must be unique
  ['property name title']: function () {
    assert.equal(this.test.title, 'property name title');
  },

  testWithNoTitle: function () {
    assert.equal(this.test.title, 'testWithNoTitle')
  }

}