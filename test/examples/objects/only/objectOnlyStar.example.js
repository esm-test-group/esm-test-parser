import assert from 'node:assert';

let testsRun;

export const ObjectOnlyStarExample = {

  beforeAll: () => testsRun = 0,

  afterAll: () => assert.equal(testsRun, 3),

  'This test will not run': function () { throw new Error(this.test.title) },

  // runs all the following tests after this statement
  only: '*',

  'test 1': () => testsRun++,

  'test 2': () => testsRun++,

  'test 3': () => testsRun++

}