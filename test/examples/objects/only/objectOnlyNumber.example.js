import assert from 'node:assert';

let testsRun;

export const ObjectOnlyNumberExample = {

  beforeAll: () => testsRun = 0,

  afterAll: () => assert.equal(testsRun, 2),

  'This test will not run': function () { throw new Error(this.test.title) },

  // runs the 2 tests following this statement
  only: '2',

  // this function will run
  'test 1': () => testsRun++,

  // this function will run
  'test 2': () => testsRun++,

  'This test will not run either': function () { throw new Error(this.test.title) },

}