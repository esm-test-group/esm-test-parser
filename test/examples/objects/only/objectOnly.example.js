export const ObjectOnlyExample = {

  // runs this test only
  only: true,
  'test 1': function () {

  },

  'This test will not run': function () { throw new Error(this.test.title) },

}