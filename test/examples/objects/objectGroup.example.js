// test/examples/objects/objectGroup.example.js
import * as assert from "node:assert";

export const ObjectGroupExample1 = {

  'test 1': function () {
    const thisGroup = this.test.parent.title
    const thisParentGroup = this.test.parent.parent.title

    assert.equal(thisGroup, "ObjectGroupExample1");
    assert.equal(thisParentGroup, "NamingModuleGroups");
  },

}