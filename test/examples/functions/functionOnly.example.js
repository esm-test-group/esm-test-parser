import * as assert from "assert";
import { only } from 'esm-test-parser';

let testsRun = 0;

test1[only] = true;
export function test1() {
  testsRun++;
}

test2.title = "This test will not run";
export function test2() { throw new Error(this.test.title) }

export function afterAll() {
  assert.equal(testsRun, 1);
}