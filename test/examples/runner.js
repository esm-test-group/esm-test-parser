import Mocha from 'mocha';
import registerMochaUiEsm from 'mocha-ui-esm';
import SourceMaps from 'source-map-support';

import { MultiReporter } from '../reporters/multiReporter.js';
import * as OnlyExampleModules from './all-only.examples.js';
import * as ExampleModules from './all.examples.js';

run();

async function run() {
  await runIsolated(ExampleModules)
  await runIsolated(OnlyExampleModules)
}

async function runIsolated(testModules) {

  // install esm test intergration
  registerMochaUiEsm();

  // install soucemap support
  SourceMaps.install();

  // create the test runner
  const runner = new Mocha({
    ui: 'esm',
    reporter: MultiReporter,
    reporterOptions: {
      jUnitFile: "./junit/examples-tests.xml",
      testSuiteNameDelimiter: "."
    },
    color: true,
    timeout: 60000,
  })

  // register tests
  runner.suite.emit('modules', testModules);

  // execute the tests
  return await new Promise((success, reject) => {
    runner.run(
      failures => {
        if (failures > 0) process.exit(failures)
        success()
      },
      {
        jobs: 1
      }
    )
  });

}