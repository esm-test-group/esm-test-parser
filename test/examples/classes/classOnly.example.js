import { testOnly } from 'esm-test-parser';

export class ClassOnlyExample {

  // runs this test only
  @testOnly()
  ['test 1']() {

  }

  ['This test will not run']() { throw new Error(this.suiteContext.test.title) }

}