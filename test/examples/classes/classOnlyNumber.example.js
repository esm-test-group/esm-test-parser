import assert from 'node:assert';
import { testOnly } from 'esm-test-parser';

export class ClassOnlyNumberExample {

  constructor () {
    this.testsRun = 0;
  }

  afterAll() {
    assert.equal(this.testsRun, 2);
  }

  ['This test will not run']() { throw new Error(this.suiteContext.test.title) }

  // runs the 2 tests following this statement
  @testOnly('2')
  ['test 1']() {
    this.testsRun++;
  }

  ['test 2']() {
    this.testsRun++;
  }

  ['This test will not run either']() { throw new Error(this.suiteContext.test.title) }

}