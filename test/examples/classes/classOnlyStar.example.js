import assert from 'node:assert';
import { testOnly } from 'esm-test-parser';

export class ClassOnlyStarExample {

  constructor () {
    this.testsRun = 0;
  }

  afterAll() {
    assert.equal(this.testsRun, 3);
  }

  ['This test will not run']() { throw new Error(this.suiteContext.test.title) }

  // runs all the following tests below this statement
  @testOnly('*')
  ['test 1']() {
    this.testsRun++;
  }

  ['test 2']() {
    this.testsRun++;
  }

  ['test 3']() {
    this.testsRun++;
  }

}