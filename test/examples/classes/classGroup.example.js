// test/examples/classes/classGroup.example.js
import assert from 'node:assert';

export class ClassGroupExample1 {

  ['test 1']() {
    const thisGroup = this.suiteContext.test.parent.title
    const thisParentGroup = this.suiteContext.test.parent.parent.title

    assert.equal(thisGroup, "ClassGroupExample1");
    assert.equal(thisParentGroup, "NamingModuleGroups");
  }

}