import * as assert from 'assert';
import { testTitle } from 'esm-test-parser';

/**
 * Hierarchy output
 * 
 *  Test title examples
 *    ✔ property name title
 *    ✔ decorator title
 *    ✔ testWithNoTitle
 */
@testTitle('Test title examples')
export class ClassTestTitlesExample {

  // custom test title
  // js property names must be unique
  ['property name title']() {
    assert.equal(this.suiteContext.test.title, 'property name title');
  }

  // decorator title (needs decorator support)
  @testTitle('decorator title')
  testWithDecoratorTitle() {
    assert.equal(this.suiteContext.test.title, 'decorator title');
  }

  testWithNoTitle() {
    assert.equal(this.suiteContext.test.title, 'testWithNoTitle');
  }

  afterAll() {
    assert.equal(this.suiteContext.test.parent.title, 'Test title examples');
  }

}