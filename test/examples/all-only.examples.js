export * from './classes/classOnly.example.js';
export * from './classes/classOnlyNumber.example.js';
export * from './classes/classOnlyStar.example.js';
export * as FunctionOnlyTest from './functions/functionOnly.example.js';
export * from './objects/only/objectOnly.example.js';
export * from './objects/only/objectOnlyNumber.example.js';
export * from './objects/only/objectOnlyStar.example.js';