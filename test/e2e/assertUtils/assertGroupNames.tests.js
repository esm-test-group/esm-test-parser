import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { Test, TestType } from '../../../src/parser/models/all.js';
import { assertGroupNames } from '../utils.js';

export const AssertUtilGroupNamesTests = {

  [test.title]: assertGroupNames.name,

  'asserts true when groupNames exist': () => {
    const testProps = [
      createTestGroupProp(
        "root",
        "TestObjectGroup",
        new TestType(TestType.OBJECT)
      ),
      createTestGroupProp(
        "root",
        "TestModuleGroup",
        new TestType(TestType.MODULE)
      ),
      createTestGroupProp(
        "root",
        "TestClassInstGroup",
        new TestType(TestType.CLASS_INST)
      ),
      createTestFunctionProp("root_TestObjectGroup", "TestObjectFunction"),
      createTestFunctionProp("root_TestClassInstGroup", "TestClassInstFunction"),
      createTestFunctionProp("root_TestModuleGroup", "TestModuleFunction")
    ];

    const testGroupMap = {
      "root_TestObjectGroup": {},
      "root_TestModuleGroup": {},
      "root_TestClassInstGroup": {},
    };

    const expectedGroupNames = Object.keys(testGroupMap);

    assertGroupNames(testProps, expectedGroupNames);
  },

  'asserts false when groupNames are missing': () => {
    const testProps = [
      createTestGroupProp(
        "root",
        "TestObjectGroup",
        new TestType(TestType.OBJECT)
      ),
      createTestGroupProp(
        "root",
        "TestClassInstGroup",
        new TestType(TestType.CLASS_INST)
      ),
      createTestFunctionProp("root_TestObjectGroup", "TestObjectFunction"),
      createTestFunctionProp("root_TestClassInstGroup", "TestClassInstFunction"),
      createTestFunctionProp("root_TestModuleGroup", "TestModuleFunction")
    ];

    const testGroupMap = {
      "root_TestObjectGroup": {},
      "root_TestModuleGroup": {},
      "root_TestClassInstGroup": {},
    };

    const expectedGroupNames = Object.keys(testGroupMap);
    assert.throws(
      () => assertGroupNames(testProps, expectedGroupNames),
      {
        name: 'AssertionError'
      }
    );
  },

}

function createTestFunctionProp(groupName, name) {
  return new Test(
    groupName,
    name,
    {},
    new TestType(TestType.FUNCTION),
    [],
    ""
  )
}

function createTestGroupProp(groupName, name, testType) {
  return new Test(
    groupName,
    name,
    {},
    testType,
    [],
    ""
  )
}