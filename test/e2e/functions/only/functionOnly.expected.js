import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const FunctionOnlyExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'FunctionOnlyFixture', type: TestType.MODULE },
    { name: 'test1', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'test2', type: TestType.FUNCTION },
  ]
);
