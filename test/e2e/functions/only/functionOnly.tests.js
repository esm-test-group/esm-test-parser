import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { FunctionOnlyExpectedProps } from './functionOnly.expected.js';
import * as FunctionOnlyFixture from './functionOnly.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const FunctionOnly = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from FunctionOnlyFixture': () => {
    const actualProps = extractTestsFromModule(
      { FunctionOnlyFixture: createModule(FunctionOnlyFixture) },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, FunctionOnlyExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}