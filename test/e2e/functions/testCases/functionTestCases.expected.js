import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const FunctionTestCasesExpectedProps = createExpectedProps(
  [
    { name: 'FunctionTestCasesFixture', type: TestType.MODULE },
    { name: 'FunctionTestCasesFixture -> testCases (case 1)', type: TestType.FUNCTION_WITH_CASES_TITLE },
    { name: 'FunctionTestCasesFixture -> testCases (case 2)', type: TestType.FUNCTION_WITH_CASES_TITLE },
    { name: 'FunctionSingleArgumentTestCasesFixture -> singleArgTestCases (case 1)', type: TestType.FUNCTION_WITH_CASES_TITLE },
    { name: 'FunctionSingleArgumentTestCasesFixture -> singleArgTestCases (case 2)', type: TestType.FUNCTION_WITH_CASES_TITLE },
    { name: 'FunctionPropertyTestCasesKeyFixture -> literalTestCases (case 1)', type: TestType.FUNCTION_WITH_CASES_TITLE },
    { name: 'FunctionPropertyTestCasesKeyFixture -> literalTestCases (case 2)', type: TestType.FUNCTION_WITH_CASES_TITLE },
  ]
);
