import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../../utils.js';
import { FunctionTestCasesExpectedProps } from './functionTestCases.expected.js';
import * as FunctionTestCasesFixture from './functionTestCases.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const FunctionTestCases = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from FunctionTestCasesFixture': () => {
    const actualProps = extractTestsFromModule(
      { FunctionTestCasesFixture: createModule(FunctionTestCasesFixture) },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, FunctionTestCasesExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

}