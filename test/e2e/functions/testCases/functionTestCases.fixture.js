import { test } from "../../../../src/index.js";

test1[test.title] = "FunctionTestCasesFixture -> testCases (case $i)";
test1[test.cases] = [
  [1, 2, 3],
  [4, 5, 6]
];
export function test1(...boundArgs) {
  return {
    thisArg: this,
    boundArgs
  }
}

test2[test.title] = "FunctionSingleArgumentTestCasesFixture -> singleArgTestCases (case $i)";
test2[test.cases] = [
  1,
  2,
];
export function test2(...boundArgs) {
  return {
    thisArg: this,
    boundArgs
  }
}

test3[test.title] = "FunctionPropertyTestCasesKeyFixture -> literalTestCases (case $i)";
test3["testCases"] = [
  [1, 2, 3],
  [4, 5, 6]
];
export function test3(...boundArgs) {
  return {
    thisArg: this,
    boundArgs
  }
}