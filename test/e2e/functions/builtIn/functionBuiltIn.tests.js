import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { FunctionBuiltInExpectedProps } from './functionBuiltIn.expected.js';
import * as FunctionBuiltInFixture from './functionBuiltIn.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const FunctionBuiltIn = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from FunctionBuiltInFixture': () => {
    const actualProps = extractTestsFromModule(
      { FunctionBuiltInFixture: createModule(FunctionBuiltInFixture) },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, FunctionBuiltInExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}