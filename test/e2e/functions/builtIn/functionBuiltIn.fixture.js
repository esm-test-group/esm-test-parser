export function afterAll() { return { thisArg: this } }

export function afterEach() { return { thisArg: this } }

export function beforeAll() { return { thisArg: this } }

export function beforeEach() { return { thisArg: this } }

export function test1() { return { thisArg: this } }

export function test2() { return { thisArg: this } }