import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const FunctionBuiltInExpectedProps = createExpectedProps(
  [
    { name: 'FunctionBuiltInFixture', type: TestType.MODULE },
    { name: 'afterAll', type: TestType.BUILTIN_FUNCTION },
    { name: 'afterEach', type: TestType.BUILTIN_FUNCTION },
    { name: 'beforeAll', type: TestType.BUILTIN_FUNCTION },
    { name: 'beforeEach', type: TestType.BUILTIN_FUNCTION },
    { name: 'test1', type: TestType.FUNCTION },
    { name: 'test2', type: TestType.FUNCTION },
  ]
);