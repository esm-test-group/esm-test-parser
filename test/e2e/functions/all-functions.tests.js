export * from './basic/functionBasic.tests.js';
export * from './builtIn/functionBuiltIn.tests.js';
export * from './only/functionOnly.tests.js';
export * from './testCases/functionTestCases.tests.js';
export * from './title/functionTitle.tests.js';