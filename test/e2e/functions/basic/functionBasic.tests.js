import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { FunctionBasicExpectedProps } from './functionBasic.expected.js';
import * as FunctionBasicFixture from './functionBasic.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const FunctionBasic = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from FunctionBasicFixture': () => {
    const actualProps = extractTestsFromModule(
      { FunctionBasicFixture: createModule(FunctionBasicFixture) },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, FunctionBasicExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}