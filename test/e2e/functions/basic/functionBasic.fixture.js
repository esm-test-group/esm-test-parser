export function test1() { return { thisArg: this } }

export function test2() { return { thisArg: this } }

export function test3(done) { return { thisArg: this, done } }