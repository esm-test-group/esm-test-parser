import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const FunctionBasicExpectedProps = createExpectedProps(
  [
    { name: 'FunctionBasicFixture', type: TestType.MODULE },
    { name: 'test1', type: TestType.FUNCTION },
    { name: 'test2', type: TestType.FUNCTION },
    { name: 'test3', type: TestType.FUNCTION },
  ]
);
