import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { FunctionTitleExpectedProps } from './functionTitle.expected.js';
import * as FunctionTitleFixture from './functionTitle.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const FunctionTitle = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from FunctionTitleFixture': () => {
    const actualProps = extractTestsFromModule(
      { FunctionTitleFixture: createModule(FunctionTitleFixture) },
      testOptions
    )

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    assertProperties(actualProps, FunctionTitleExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}