import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const FunctionTitleExpectedProps = createExpectedProps(
  [
    { name: 'FunctionTitleFixture', type: TestType.MODULE },
    { name: 'FunctionTitleFixture -> test 1', type: TestType.FUNCTION_WITH_TITLE },
    { name: 'FunctionTitleFixture -> test 2', type: TestType.FUNCTION_WITH_TITLE },
  ]
);
