import * as assert from 'assert';
import { TestType } from '../../src/parser/models/testType.js';
import { isModule } from '../../src/parser/utils/moduleUtils.js';

export function createExpectedProps(expectedProps) {
  return expectedProps.map(
    p => ({
      name: p.name,
      type: new TestType(p.type)
    })
  )
}

export function assertProperties(actualProps, expectedProps) {

  assert.equal(
    actualProps.length,
    expectedProps.length,
    "actual properties length did not match expected properties length.",
    + `\r\nactual length was ${actualProps.length}.`
    + `\r\n expected length was ${expectedProps.length}.`
  );

  let iterated = 0;

  expectedProps.forEach((expected, index) => {
    const actualTest = actualProps[index];

    assert.equal(
      actualTest.name,
      expected.name,
      `Test name for index ${index} failed.`
      + `\r\nExpected name to be ${expected.name}`
      + `\r\nActual name was ${actualTest.name}`
    );

    assert.deepEqual(
      actualTest.type,
      expected.type,
      `Property type for index ${index} failed for expected property '${expected.name}'.`
      + `\r\nExpected type to be ${expected.type}`
      + `\r\nActual type was ${actualTest.type}`
    );

    iterated++;
  });

  assert.equal(actualProps.length, iterated);
}

export function assertGroupNames(testProperties, expectedGroupNames) {
  const actualGroupMap = getGroupMap(testProperties);
  const actualGroupNames = Object.keys(actualGroupMap);

  assert.ok(
    actualGroupNames.length > 0,
    "No group names found in the test properties"
  );

  assert.deepEqual(actualGroupNames, expectedGroupNames);

  let checked = 0;

  testProperties.forEach(prop => {
    if (prop.type.has(TestType.FUNCTION)) {
      const groupName = prop.groupName;
      assert.ok(
        actualGroupMap[groupName] instanceof Object,
        `Could not find group '${prop.groupName} for function '${prop.name}'`
      );
      checked++;
    }
  });

  assert.ok(
    checked > 0,
    "No function groupName was checked"
  );
}

export function assertFunctions(actualProps, expectedOptions) {
  let checked = 0;

  const testFunctions = actualProps
    .filterHas(TestType.FUNCTION)
    .filter(x => x.type.has(TestType.HAS_CASES) === false);

  assert.ok(
    testFunctions.length > 0,
    "No test functions were found in the TestCollection"
  );

  testFunctions.forEach((actualTest, index) => {
    const hasDoneArg = actualTest.value.length > 0;
    const testFnResult = hasDoneArg
      ? actualTest.value(1)
      : actualTest.value();

    const actualThis = testFnResult.thisArg;
    const isClassInst = actualTest.type.has(TestType.CLASS_INST_FUNCTION);

    const actualContext = isClassInst
      ? actualThis[expectedOptions.classContextPropertyName]
      : actualThis;

    const expectedContext = expectedOptions.testContextLookup(
      actualTest.groupName
    );

    assert.deepEqual(
      actualContext,
      expectedContext,
      `Context given to '${actualTest.name}' function for index ${index} failed.`
      + `\r\nThisArg context did not match options.testContextLookup`
    )

    if (hasDoneArg) {
      assert.ok(Object.hasOwn(testFnResult, "done"), "Done argument does not exist");
      assert.equal(1, testFnResult.done, "Done argument does not match");
    }

    checked++;
  });

  assert.equal(checked, testFunctions.length);
}

export function assertTestCaseFunctions(actualProps) {
  let checked = 0;

  const testCases = actualProps.filterHas(TestType.HAS_CASES);

  assert.ok(testCases.length > 0, "No test cases were found in the TestCollection");

  testCases.forEach((actualTest, index) => {
    const hasDoneArg = actualTest.value.length > 0;
    const testCaseFnResult = hasDoneArg
      ? actualTest.value(1)
      : actualTest.value();

    const actualCaseIndex = testCaseFnResult.boundArgs[testCaseFnResult.boundArgs.length - 1];
    const actualCaseArgs = testCaseFnResult.boundArgs.slice(0, testCaseFnResult.boundArgs.length - 1)
    const expectedArgs = actualTest.testCases[actualCaseIndex];

    assert.deepEqual(
      actualCaseArgs,
      expectedArgs,
      `TestCases for '${actualTest.name}' function at index ${index} failed.`
      + `\r\nExpected args to be ${expectedArgs}`
      + `\r\nActual args was ${actualCaseArgs}`
    );

    if (hasDoneArg) {
      assert.ok(Object.hasOwn(testCaseFnResult, "done"), "Done argument does not exist");
      assert.equal(1, testCaseFnResult.done, "Done argument does not match");
    }

    checked++;
  });

  assert.equal(checked, testCases.length);
}

export function getGroupMap(testProperties) {
  const groupMap = {};

  testProperties.forEach(prop => {
    if (prop.isGroup) {
      const testGroupName = prop.groupName + "_" + prop.name;
      groupMap[testGroupName] = {
        testGroupName
      };
    }
  })

  return groupMap;
}

export function createModule(testModule) {
  if (isModule(testModule)) return testModule;

  Object.defineProperty(
    testModule,
    Symbol.toStringTag,
    {
      value: 'Module'
    }
  )

  return testModule;
}