import { extractTestsFromModule } from '../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../utils.js';
import * as ObjectModuleExpectedProps from './all-objects.expected.js';
import * as ObjectTestModules from './all-objects.fixtures.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectsFixtures = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts all tests from object module fixture': () => {
    const expectedProps = [];

    Object.keys(ObjectModuleExpectedProps)
      .forEach(
        k => expectedProps.push.apply(
          expectedProps,
          ObjectModuleExpectedProps[k]
        )
      );

    const actualProps = extractTestsFromModule(
      createModule(ObjectTestModules),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    // assert name, type and ordering match expected
    assertProperties(actualProps, expectedProps)

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

  "Extracts group names from object module fixture": () => {
    testOptions.sort = "groupsOnly";

    const expectedNames = [
      "root_object property title fixture",
      "root_object title fixture",
      "root_ObjectBasicFixture",
      "root_ObjectBuiltInFixture",
      "root_ObjectDefaultExportFixture",
      "root_ObjectGroupingFixture",
      "root_ObjectOnlyFixture",
      "root_ObjectOnlyGroupFixture",
      "root_ObjectOnlyGroupFunctionFixture",
      "root_ObjectOnlyNumFixture",
      "root_ObjectOnlyPropertyKeyFixture",
      "root_ObjectOnlyStarFixture",
      "root_ObjectOnlyStarGroupingFixture",
      "root_ObjectOnlyWithCasesFixture",
      "root_ObjectSingleTestCasesFixture",
      "root_ObjectTestCasesFixture",
      "root_ObjectTestCasesWithDoneFixture",
      "root_ObjectTitleGroupingFixture",
      "root_ObjectTitleTestCaseArgReplaceFixture -> object title test cases fixture",
      "root_ObjectGroupingFixture_ObjectGroupingFixture -> level 1",
      "root_ObjectGroupingFixture_ObjectGroupingFixture -> level 1_ObjectGroupingFixture -> level 2",
      "root_ObjectOnlyGroupFixture_ObjectOnlyGroupFixture -> group 1",
      "root_ObjectOnlyGroupFunctionFixture_test group 1",
      "root_ObjectOnlyStarGroupingFixture_ObjectOnlyStarGroupingFixture -> test group 1",
      "root_ObjectOnlyStarGroupingFixture_ObjectOnlyStarGroupingFixture -> test group 2",
      "root_ObjectTitleGroupingFixture_ObjectTitleGroupingFixture -> object title fixture"
    ]

    const testProperties = extractTestsFromModule(
      ObjectTestModules,
      testOptions
    )

    assertGroupNames(testProperties, expectedNames);
  }

}