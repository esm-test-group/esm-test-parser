export * from './basic/objectBasic.fixture.js';
export * from './builtIn/objectBuiltIn.fixture.js';
export * as ObjectDefaultExportFixture from './defaultExport/objectDefaultExport.fixture.js';
export * from './grouping/objectGrouping.fixture.js';
export * from './only/objectOnly.fixture.js';
export * from './testCases/objectTestCases.fixture.js';
export * from './title/objectTitle.fixture.js';