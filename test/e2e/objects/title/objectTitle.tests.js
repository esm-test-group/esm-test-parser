import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../../utils.js';
import {
  ObjectPropertyTitleKeyExpectedProps,
  ObjectTitleExpectedProps,
  ObjectTitleGroupingExpectedProps,
  ObjectTitleTestCaseArgReplaceExpectedProps
} from './objectTitle.expected.js';
import {
  ObjectPropertyTitleKeyFixture,
  ObjectTitleFixture,
  ObjectTitleGroupingFixture,
  ObjectTitleTestCaseArgReplaceFixture
} from './objectTitle.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectTitle = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectTitleFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectTitleFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectTitleExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectTitleGroupingFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectTitleGroupingFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectTitleGroupingExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectTitleTestCaseArgReplaceFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectTitleTestCaseArgReplaceFixture },
      Object.assign(
        {},
        testOptions
      )
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectTitleTestCaseArgReplaceExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test case functions
    assertTestCaseFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectPropertyTitleKeyFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectPropertyTitleKeyFixture },
      Object.assign(
        {},
        testOptions
      )
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectPropertyTitleKeyExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },



}