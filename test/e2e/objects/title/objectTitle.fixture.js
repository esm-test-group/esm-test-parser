import { test } from "../../../../src/index.js"

export const ObjectTitleFixture = {
  [test.title]: "object title fixture",
  'ObjectTitleFixture -> test 1': function () { return { thisArg: this } },
  'ObjectTitleFixture -> test 2': function () { return { thisArg: this } }
}

export const ObjectTitleGroupingFixture = {
  childGroup: {
    [test.title]: "ObjectTitleGroupingFixture -> object title fixture",
    'ObjectTitleGroupingFixture -> child group -> test 1': function () { return { thisArg: this } },
    'ObjectTitleGroupingFixture -> child group -> test 2': function () { return { thisArg: this } }
  }
}

export const ObjectTitleTestCaseArgReplaceFixture = {
  [test.title]: "ObjectTitleTestCaseArgReplaceFixture -> object title test cases fixture",
  'ObjectTitleTestCaseArgReplaceFixture -> test case $1 (case $i)': [
    1,
    2,
    3,
    4,
    function (...boundArgs) {
      return {
        thisArg: this,
        boundArgs
      }
    }
  ]
}

export const ObjectPropertyTitleKeyFixture = {
  title: "object property title fixture",
  'ObjectPropertyTitleKeyFixture -> test 1': function () { return { thisArg: this } },
  'ObjectPropertyTitleKeyFixture -> test 2': function () { return { thisArg: this } }
}