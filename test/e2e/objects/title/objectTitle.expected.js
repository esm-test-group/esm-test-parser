import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectTitleExpectedProps = createExpectedProps(
  [
    // title
    { name: 'object title fixture', type: TestType.OBJECT },
    { name: 'ObjectTitleFixture -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectTitleFixture -> test 2', type: TestType.FUNCTION },
  ]
);

export const ObjectTitleGroupingExpectedProps = createExpectedProps(
  [
    // child group title
    { name: 'ObjectTitleGroupingFixture', type: TestType.OBJECT },
    { name: 'ObjectTitleGroupingFixture -> object title fixture', type: TestType.OBJECT },
    { name: 'ObjectTitleGroupingFixture -> child group -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectTitleGroupingFixture -> child group -> test 2', type: TestType.FUNCTION },
  ]
);

export const ObjectTitleTestCaseArgReplaceExpectedProps = createExpectedProps(
  [
    // test case arg replace
    { name: 'ObjectTitleTestCaseArgReplaceFixture -> object title test cases fixture', type: TestType.OBJECT },
    { name: 'ObjectTitleTestCaseArgReplaceFixture -> test case 1 (case 1)', type: TestType.FUNCTION_WITH_CASES },
    { name: 'ObjectTitleTestCaseArgReplaceFixture -> test case 2 (case 2)', type: TestType.FUNCTION_WITH_CASES },
    { name: 'ObjectTitleTestCaseArgReplaceFixture -> test case 3 (case 3)', type: TestType.FUNCTION_WITH_CASES },
    { name: 'ObjectTitleTestCaseArgReplaceFixture -> test case 4 (case 4)', type: TestType.FUNCTION_WITH_CASES },
  ]
);

export const ObjectPropertyTitleKeyExpectedProps = createExpectedProps(
  [
    // custom title key taken from symbol test
    { name: 'object property title fixture', type: TestType.OBJECT },
    { name: 'ObjectPropertyTitleKeyFixture -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectPropertyTitleKeyFixture -> test 2', type: TestType.FUNCTION },
  ]
);