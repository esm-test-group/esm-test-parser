import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectBuiltInExpectedProps = createExpectedProps(
  [
    { name: 'ObjectBuiltInFixture', type: TestType.OBJECT },
    { name: 'beforeAll', type: TestType.BUILTIN_FUNCTION },
    { name: 'beforeEach', type: TestType.BUILTIN_FUNCTION },
    { name: 'afterAll', type: TestType.BUILTIN_FUNCTION },
    { name: 'afterEach', type: TestType.BUILTIN_FUNCTION },
    { name: 'ObjectBuiltInFixture -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectBuiltInFixture -> test 2', type: TestType.FUNCTION },
  ]
);