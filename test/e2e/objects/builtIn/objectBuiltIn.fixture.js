export const ObjectBuiltInFixture = {

  beforeAll: function () { return { thisArg: this } },

  beforeEach: function () { return { thisArg: this } },

  afterAll: function () { return { thisArg: this } },

  afterEach: function () { return { thisArg: this } },

  'ObjectBuiltInFixture -> test 1': function () { return { thisArg: this } },

  'ObjectBuiltInFixture -> test 2': function () { return { thisArg: this } }

}