import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ObjectBuiltInExpectedProps } from './objectBuiltIn.expected.js';
import { ObjectBuiltInFixture } from './objectBuiltIn.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectBuiltIn = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectBuiltInFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectBuiltInFixture }),
      testOptions
    )

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    assertProperties(actualProps, ObjectBuiltInExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}