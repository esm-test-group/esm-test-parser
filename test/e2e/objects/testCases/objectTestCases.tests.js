import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../../utils.js';
import {
  ObjectSingleTestCasesExpectedProps,
  ObjectTestCasesExpectedProps
} from './objectTestCases.expected.js';
import {
  ObjectSingleTestCasesFixture,
  ObjectTestCasesFixture
} from './objectTestCases.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectTestCases = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectTestCasesFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectTestCasesFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectTestCasesExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

  'Extracts tests from ObjectSingleTestCasesFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectSingleTestCasesFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectSingleTestCasesExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

}