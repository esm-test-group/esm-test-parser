export const ObjectTestCasesFixture = {

  'ObjectTestCasesFixture -> testCase 1 (case $i)': [
    [1, 2, 3],
    [4, 5, 6],
    function (...boundArgs) {
      return {
        thisArg: this,
        boundArgs
      }
    }
  ]

}

export const ObjectSingleTestCasesFixture = {

  'ObjectSingleTestCasesFixture -> testCase 1 (case $i)': [
    1,
    2,
    function (...boundArgs) {
      return {
        thisArg: this,
        boundArgs
      }
    }
  ]

}

export const ObjectTestCasesWithDoneFixture = {

  'ObjectTestCasesWithDoneFixture -> testCase 1 (case $i)': [
    1,
    2,
    function (arg1, caseIndex, done) {
      return {
        thisArg: this,
        boundArgs: [arg1, caseIndex],
        done
      }
    }
  ]

}