import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectTestCasesExpectedProps = createExpectedProps(
  [
    { name: 'ObjectTestCasesFixture', type: TestType.OBJECT },
    { name: 'ObjectTestCasesFixture -> testCase 1 (case 1)', type: TestType.FUNCTION_WITH_CASES },
    { name: 'ObjectTestCasesFixture -> testCase 1 (case 2)', type: TestType.FUNCTION_WITH_CASES },
  ]
);

export const ObjectSingleTestCasesExpectedProps = createExpectedProps(
  [
    { name: 'ObjectSingleTestCasesFixture', type: TestType.OBJECT },
    { name: 'ObjectSingleTestCasesFixture -> testCase 1 (case 1)', type: TestType.FUNCTION_WITH_CASES },
    { name: 'ObjectSingleTestCasesFixture -> testCase 1 (case 2)', type: TestType.FUNCTION_WITH_CASES },
  ]
);

export const ObjectTestCasesWithDoneExpectedProps = createExpectedProps(
  [
    { name: 'ObjectTestCasesWithDoneFixture', type: TestType.OBJECT },
    { name: 'ObjectTestCasesWithDoneFixture -> testCase 1 (case 1)', type: TestType.FUNCTION_WITH_CASES },
    { name: 'ObjectTestCasesWithDoneFixture -> testCase 1 (case 2)', type: TestType.FUNCTION_WITH_CASES },
  ]
);