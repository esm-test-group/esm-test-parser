export * from './basic/objectBasic.tests.js';
export * from './builtIn/objectBuiltIn.tests.js';
export * as ObjectDefaultExportFixture from './defaultExport/objectDefaultExport.tests.js';
export * from './grouping/objectGrouping.tests.js';
export * from './only/objectOnly.tests.js';
export * from './testCases/objectTestCases.tests.js';
export * from './title/objectTitle.tests.js';