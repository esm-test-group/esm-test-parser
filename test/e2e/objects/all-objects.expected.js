export * from './basic/objectBasic.expected.js';
export * from './builtIn/objectBuiltIn.expected.js';
export * from './defaultExport/objectDefaultExport.expected.js';
export * from './grouping/objectGrouping.expected.js';
export * from './only/objectOnly.expected.js';
export * from './testCases/objectTestCases.expected.js';
export * from './title/objectTitle.expected.js';