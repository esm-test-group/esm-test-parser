import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ObjectDefaultExportFixture } from '../all-objects.fixtures.js';
import { ObjectDefaultExportExpectedProps } from './objectDefaultExport.expected.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectDefaultExport = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectDefaultExportFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectDefaultExportFixture: createModule(ObjectDefaultExportFixture) },
      testOptions
    )

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    assertProperties(actualProps, ObjectDefaultExportExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}