import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectDefaultExportExpectedProps = createExpectedProps(
  [
    { name: 'ObjectDefaultExportFixture', type: TestType.MODULE },
    { name: 'ObjectDefaultExportFixture -> test 1', type: TestType.FUNCTION },
  ]
);