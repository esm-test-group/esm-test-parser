import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ObjectGroupingExpectedProps } from './objectGrouping.expected.js';
import { ObjectGroupingFixture } from './objectGrouping.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectGrouping = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectGroupingFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectGroupingFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectGroupingExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}