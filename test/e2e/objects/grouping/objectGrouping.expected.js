import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectGroupingExpectedProps = createExpectedProps(
  [
    { name: 'ObjectGroupingFixture', type: TestType.OBJECT },
    { name: 'ObjectGroupingFixture -> root -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectGroupingFixture -> root -> test 2', type: TestType.FUNCTION },
    { name: 'ObjectGroupingFixture -> level 1', type: TestType.OBJECT },
    { name: 'ObjectGroupingFixture -> level 1 -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectGroupingFixture -> level 1 -> test 2', type: TestType.FUNCTION },
    { name: 'ObjectGroupingFixture -> level 2', type: TestType.OBJECT },
    { name: 'ObjectGroupingFixture -> level 2 -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectGroupingFixture -> level 2 -> test 2', type: TestType.FUNCTION },
  ]
);