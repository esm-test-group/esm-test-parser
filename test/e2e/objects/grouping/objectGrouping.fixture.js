export const ObjectGroupingFixture = {
  'ObjectGroupingFixture -> root -> test 1': function () { return { thisArg: this } },
  'ObjectGroupingFixture -> root -> test 2': function () { return { thisArg: this } },
  'ObjectGroupingFixture -> level 1': {
    'ObjectGroupingFixture -> level 1 -> test 1': function () { return { thisArg: this } },
    'ObjectGroupingFixture -> level 1 -> test 2': function () { return { thisArg: this } },
    'ObjectGroupingFixture -> level 2': {
      'ObjectGroupingFixture -> level 2 -> test 1': function () { return { thisArg: this } },
      'ObjectGroupingFixture -> level 2 -> test 2': function () { return { thisArg: this } },
    }
  }
}