import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectBasicExpectedProps = createExpectedProps(
  [
    { name: 'ObjectBasicFixture', type: TestType.OBJECT },
    { name: 'ObjectBasicFixture -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectBasicFixture -> test 2', type: TestType.FUNCTION },
    { name: 'ObjectBasicFixture -> test 3', type: TestType.FUNCTION },
  ]
);
