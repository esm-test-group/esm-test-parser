import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ObjectBasicExpectedProps } from './objectBasic.expected.js';
import { ObjectBasicFixture } from './objectBasic.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectBasic = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectBasicFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectBasicFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectBasicExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}