export const ObjectBasicFixture = {
  'ObjectBasicFixture -> test 1': function () { return { thisArg: this } },
  'ObjectBasicFixture -> test 2': function () { return { thisArg: this } },
  'ObjectBasicFixture -> test 3': function (done) { return { thisArg: this, done } }
};