import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ObjectOnlyExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'ObjectOnlyFixture', type: TestType.OBJECT },
    { name: 'ObjectOnlyFixture -> test 1', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyFixture -> test 2', type: TestType.FUNCTION },
  ]
);

export const ObjectOnlyStarExpectedProps = createExpectedProps(
  [
    // only: '*'
    { name: 'ObjectOnlyStarFixture', type: TestType.OBJECT },
    { name: 'beforeAll', type: TestType.BUILTIN_FUNCTION },
    { name: 'ObjectOnlyStarFixture -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectOnlyStarFixture -> test 2', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyStarFixture -> test 3', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyStarFixture -> test 4', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'afterAll', type: TestType.BUILTIN_FUNCTION },
  ]
);

export const ObjectOnlyNumExpectedProps = createExpectedProps(
  [
    // only: '2'
    { name: 'ObjectOnlyNumFixture', type: TestType.OBJECT },
    { name: 'ObjectOnlyNumFixture -> test 1', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyNumFixture -> test 2', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyNumFixture -> test 3', type: TestType.FUNCTION },
  ]
);

export const ObjectOnlyGroupExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'ObjectOnlyGroupFixture', type: TestType.OBJECT },
    { name: 'ObjectOnlyGroupFixture -> group 1', type: TestType.OBJECT_WITH_ONLY },
    { name: 'ObjectOnlyGroupFixture -> group 1 -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectOnlyGroupFixture -> group 1 -> test 2', type: TestType.FUNCTION },
    { name: 'ObjectOnlyGroupFixture -> test 3', type: TestType.FUNCTION },
  ]
);

export const ObjectOnlyGroupFunctionExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'ObjectOnlyGroupFunctionFixture', type: TestType.OBJECT },
    { name: 'test group 1', type: TestType.OBJECT },
    { name: 'ObjectOnlyGroupFunctionFixture -> test group 1 -> test 1', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyGroupFunctionFixture -> test group 1 -> test 2', type: TestType.FUNCTION },
    { name: 'ObjectOnlyGroupFunctionFixture -> test 1', type: TestType.FUNCTION },
  ]
);

export const ObjectOnlyStarGroupingExpectedProps = createExpectedProps(
  [
    // only: '*'
    { name: 'ObjectOnlyStarGroupingFixture', type: TestType.OBJECT },
    { name: 'ObjectOnlyStarGroupingFixture -> test group 1', type: TestType.OBJECT_WITH_ONLY },
    { name: 'ObjectOnlyStarGroupingFixture -> test group 1 -> test 1', type: TestType.FUNCTION },
    { name: 'ObjectOnlyStarGroupingFixture -> test group 2', type: TestType.OBJECT_WITH_ONLY },
    { name: 'ObjectOnlyStarGroupingFixture -> test group 2 -> test 1', type: TestType.FUNCTION },
  ]
);

export const ObjectOnlyWithCasesExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'ObjectOnlyWithCasesFixture', type: TestType.OBJECT },
    { name: 'ObjectOnlyWithCasesFixture -> test 1 (case 1)', type: TestType.FUNCTION_WITH_CASES_ONLY },
    { name: 'ObjectOnlyWithCasesFixture -> test 2', type: TestType.FUNCTION },
  ]
);

export const ObjectOnlyPropertyKeyExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'ObjectOnlyPropertyKeyFixture', type: TestType.OBJECT },
    { name: 'ObjectOnlyPropertyKeyFixture -> test 1', type: TestType.FUNCTION_WITH_ONLY },
    { name: 'ObjectOnlyPropertyKeyFixture -> test 2', type: TestType.FUNCTION },
  ]
);