import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../../utils.js';
import {
  ObjectOnlyExpectedProps,
  ObjectOnlyGroupExpectedProps,
  ObjectOnlyGroupFunctionExpectedProps,
  ObjectOnlyNumExpectedProps, 
  ObjectOnlyPropertyKeyExpectedProps, 
  ObjectOnlyStarExpectedProps,
  ObjectOnlyStarGroupingExpectedProps,
  ObjectOnlyWithCasesExpectedProps
} from './objectOnly.expected.js';
import {
  ObjectOnlyFixture,
  ObjectOnlyGroupFixture,
  ObjectOnlyGroupFunctionFixture,
  ObjectOnlyNumFixture, 
  ObjectOnlyPropertyKeyFixture, 
  ObjectOnlyStarFixture,
  ObjectOnlyStarGroupingFixture,
  ObjectOnlyWithCasesFixture
} from './objectOnly.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ObjectOnly = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ObjectOnlyFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ObjectOnlyFixture }),
      testOptions
    )

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    assertProperties(actualProps, ObjectOnlyExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectOnlyStarFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyStarFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyStarExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectOnlyNumFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyNumFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyNumExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectOnlyGroupFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyGroupFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyGroupExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectOnlyGroupFunctionFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyGroupFunctionFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyGroupFunctionExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectOnlyStarGroupingFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyStarGroupingFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyStarGroupingExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ObjectOnlyWithCasesFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyWithCasesFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyWithCasesExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

  'Extracts tests from ObjectOnlyPropertyKeyFixture': () => {
    const actualProps = extractTestsFromModule(
      { ObjectOnlyPropertyKeyFixture },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ObjectOnlyPropertyKeyExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },
}