import { only } from "../../../../src/index.js"

export const ObjectOnlyFixture = {
  [only]: true,
  'ObjectOnlyFixture -> test 1': function () { return { thisArg: this } },
  'ObjectOnlyFixture -> test 2': function () { return { thisArg: this } }
}

export const ObjectOnlyStarFixture = {
  'beforeAll': function () { return { thisArg: this } },
  'ObjectOnlyStarFixture -> test 1': function () { return { thisArg: this } },
  [only]: '*',
  'ObjectOnlyStarFixture -> test 2': function () { return { thisArg: this } },
  'ObjectOnlyStarFixture -> test 3': function () { return { thisArg: this } },
  'ObjectOnlyStarFixture -> test 4': function () { return { thisArg: this } },
  'afterAll': function () { return { thisArg: this } },
}

export const ObjectOnlyNumFixture = {
  [only]: '2',
  'ObjectOnlyNumFixture -> test 1': function () { return { thisArg: this } },
  'ObjectOnlyNumFixture -> test 2': function () { return { thisArg: this } },
  'ObjectOnlyNumFixture -> test 3': function () { return { thisArg: this } },
}

export const ObjectOnlyGroupFixture = {
  [only]: true,
  'ObjectOnlyGroupFixture -> group 1': {
    'ObjectOnlyGroupFixture -> group 1 -> test 1': function () { return { thisArg: this } },
    'ObjectOnlyGroupFixture -> group 1 -> test 2': function () { return { thisArg: this } },
  },
  'ObjectOnlyGroupFixture -> test 3': function () { return { thisArg: this } }
}

export const ObjectOnlyGroupFunctionFixture = {
  'test group 1': {
    [only]: true,
    'ObjectOnlyGroupFunctionFixture -> test group 1 -> test 1': function () { return { thisArg: this } },
    'ObjectOnlyGroupFunctionFixture -> test group 1 -> test 2': function () { return { thisArg: this } },
  },
  'ObjectOnlyGroupFunctionFixture -> test 1': function () { return { thisArg: this } },
}

export const ObjectOnlyStarGroupingFixture = {
  [only]: '*',
  'ObjectOnlyStarGroupingFixture -> test group 1': {
    ['ObjectOnlyStarGroupingFixture -> test group 1 -> test 1']: function () { return { thisArg: this } }
  },
  'ObjectOnlyStarGroupingFixture -> test group 2': {
    ['ObjectOnlyStarGroupingFixture -> test group 2 -> test 1']: function () { return { thisArg: this } }
  }
}

export const ObjectOnlyWithCasesFixture = {
  [only]: true,
  'ObjectOnlyWithCasesFixture -> test 1 (case $i)': [
    [1, 2, 3],
    function (...boundArgs) {
      return {
        thisArg: this,
        boundArgs
      }
    }
  ],
  'ObjectOnlyWithCasesFixture -> test 2': function () { return { thisArg: this } }
}

export const ObjectOnlyPropertyKeyFixture = {
  only: true,
  'ObjectOnlyPropertyKeyFixture -> test 1': function () { return { thisArg: this } },
  'ObjectOnlyPropertyKeyFixture -> test 2': function () { return { thisArg: this } }
}