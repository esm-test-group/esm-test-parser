import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ModuleOnlyExpectedProps = createExpectedProps(
  [
    // only: true
    { name: 'ModuleOnlyFixture', type: TestType.MODULE },
    { name: 'ModuleOnlyFixture -> test 1', type: TestType.FUNCTION_WITH_TITLE_ONLY },
    { name: 'ModuleOnlyFixture -> test 2', type: TestType.FUNCTION_WITH_TITLE },
  ]
);
