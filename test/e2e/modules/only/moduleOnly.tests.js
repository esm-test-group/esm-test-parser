import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ModuleOnlyExpectedProps } from './moduleOnly.expected.js';
import * as ModuleOnlyFixture from './moduleOnly.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ModuleOnly = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ModuleOnlyFixture': () => {
    const actualProps = extractTestsFromModule(
      { ModuleOnlyFixture: createModule(ModuleOnlyFixture) },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ModuleOnlyExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}