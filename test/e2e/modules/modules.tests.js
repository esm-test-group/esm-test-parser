import { extractTestsFromModule } from '../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  getGroupMap
} from '../utils.js';
import * as ModuleExpectedProps from './all-modules.expected.js';
import * as ModuleTestModules from './all-modules.fixtures.js';

let testGroupMap = {};
let testOptions = {};

export const ModulesFixtures = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts all tests from module fixture': () => {
    const expectedProps = [];

    Object.keys(ModuleExpectedProps)
      .forEach(
        k => expectedProps.push.apply(
          expectedProps,
          ModuleExpectedProps[k]
        )
      );

    const actualProps = extractTestsFromModule(ModuleTestModules, testOptions);

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    // assert name, type and ordering match expected
    assertProperties(actualProps, expectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  "Extracts suite names from module fixture": () => {
    testOptions.sort = "groupsOnly";

    const expectedNames = [
      "root_Module Title Fixture",
      "root_ModuleOnlyFixture",
      "root_Module Title Fixture_ModuleTitleObjectTest"
    ]

    const testProperties = extractTestsFromModule(
      ModuleTestModules,
      testOptions
    )

    assertGroupNames(testProperties, expectedNames);
  }

}