import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ModuleTitleExpectedProps } from './moduleTitle.expected.js';
import * as ModuleTitleFixture from './moduleTitle.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ModuleTitle = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ModuleTitleFixture': () => {
    const actualProps = extractTestsFromModule(
      { ModuleTitleFixture: createModule(ModuleTitleFixture) },
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ModuleTitleExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}