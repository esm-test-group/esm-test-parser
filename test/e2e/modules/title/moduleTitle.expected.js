import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ModuleTitleExpectedProps = createExpectedProps(
  [
    { name: 'Module Title Fixture', type: TestType.MODULE },
    { name: 'ModuleTitleObjectTest', type: TestType.OBJECT },
    { name: 'ModuleTitleObjectTest -> test 1', type: TestType.FUNCTION },
  ]
);
