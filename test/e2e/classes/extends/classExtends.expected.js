import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassExtendsExpectedProps = createExpectedProps(
  [
    { name: 'ClassExtendsFixture', type: TestType.CLASS_DEF },
    { name: 'ClassExtendsFixture -> test 1', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
    { name: 'ClassExtendsFixture -> test 2', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
    { name: 'ClassExtendsFixture -> Abstact -> extend test', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
  ]
);