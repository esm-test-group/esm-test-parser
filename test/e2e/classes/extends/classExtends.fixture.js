import { testTitle } from '../../../../src/decorators/all.js';

class AbstractTest {

  @testTitle('ClassExtendsFixture -> Abstact -> extend test')
  extendedTest() {
    return {
      thisArg: this
    }
  }

}

export class ClassExtendsFixture extends AbstractTest {

  @testTitle('ClassExtendsFixture -> test 1')
  test1() {
    return {
      thisArg: this
    }
  }

  @testTitle('ClassExtendsFixture -> test 2')
  test2() {
    return {
      thisArg: this
    }
  }

}