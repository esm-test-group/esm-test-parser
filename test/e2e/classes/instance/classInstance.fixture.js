export const ClassInstanceFixture = new class {

  ['ClassInstanceFixture -> test 1']() {
    return {
      thisArg: this
    }
  }

  ['ClassInstanceFixture -> test 2']() {
    return {
      thisArg: this
    }
  }

  ['ClassInstanceFixture -> test 3'](done) {
    return {
      thisArg: this,
      done
    }
  }

}