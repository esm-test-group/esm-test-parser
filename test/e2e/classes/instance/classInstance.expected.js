import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassInstanceExpectedProps = createExpectedProps(
  [
    { name: 'ClassInstanceFixture', type: TestType.CLASS_INST },
    { name: 'ClassInstanceFixture -> test 1', type: TestType.CLASS_INST_FUNCTION },
    { name: 'ClassInstanceFixture -> test 2', type: TestType.CLASS_INST_FUNCTION },
    { name: 'ClassInstanceFixture -> test 3', type: TestType.CLASS_INST_FUNCTION },
  ]
);
