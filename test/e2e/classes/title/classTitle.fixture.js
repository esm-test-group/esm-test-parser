import { testTitle } from '../../../../src/decorators/all.js';

@testTitle("ClassTitleFixture -> Title")
export class ClassTitleFixture {

  @testTitle('ClassTitleFixture -> test 1')
  test1() {
    return {
      thisArg: this
    }
  }

  @testTitle('ClassTitleFixture -> test 2')
  test2() {
    return {
      thisArg: this
    }
  }

}

@testTitle("ClassTitleWithBuiltInFixture -> Title")
export class ClassTitleWithBuiltInFixture {

  beforeEach() {
    return {
      thisArg: this
    }
  }

  @testTitle('ClassTitleWithBuiltInFixture -> test 1')
  test1() {
    return {
      thisArg: this
    }
  }

  @testTitle('ClassTitleWithBuiltInFixture -> test 2')
  test2() {
    return {
      thisArg: this
    }
  }

}