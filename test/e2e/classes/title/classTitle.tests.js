import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import {
  ClassTitleExpectedProps,
  ClassTitleWithBuiltInExpectedProps
} from './classTitle.expected.js';
import {
  ClassTitleFixture,
  ClassTitleWithBuiltInFixture
} from './classTitle.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ClassTitle = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ClassTitleFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassTitleFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassTitleExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ClassTitleWithBuiltInFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassTitleWithBuiltInFixture }),
      testOptions
    );

    // populate the group map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassTitleWithBuiltInExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}