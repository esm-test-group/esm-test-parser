import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassTitleExpectedProps = createExpectedProps(
  [
    { name: 'ClassTitleFixture -> Title', type: TestType.CLASS_DEF_WITH_TITLE },
    { name: 'ClassTitleFixture -> test 1', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
    { name: 'ClassTitleFixture -> test 2', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
  ]
);

export const ClassTitleWithBuiltInExpectedProps = createExpectedProps(
  [
    { name: 'ClassTitleWithBuiltInFixture -> Title', type: TestType.CLASS_DEF_WITH_TITLE },
    { name: 'beforeEach', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
    { name: 'ClassTitleWithBuiltInFixture -> test 1', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
    { name: 'ClassTitleWithBuiltInFixture -> test 2', type: TestType.CLASS_INST_FUNCTION_WITH_TITLE },
  ]
);