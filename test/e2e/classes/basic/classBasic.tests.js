import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ClassBasicExpectedProps } from './classBasic.expected.js';
import { ClassBasicFixture } from './classBasic.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ClassBasic = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ClassBasicFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassBasicFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassBasicExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}