import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassBasicExpectedProps = createExpectedProps(
  [
    { name: 'ClassBasicFixture', type: TestType.CLASS_DEF },
    { name: 'ClassBasicFixture -> test 1', type: TestType.CLASS_INST_FUNCTION },
    { name: 'ClassBasicFixture -> test 2', type: TestType.CLASS_INST_FUNCTION },
  ]
);
