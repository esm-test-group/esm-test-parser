export * from './basic/classBasic.expected.js';
export * from './builtIn/classBuiltIn.expected.js';
export * from './defaultExport/classDefaultExport.expected.js';
export * from './extends/classExtends.expected.js';
export * from './instance/classInstance.expected.js';
export * from './only/classOnly.expected.js';
export * from './testCases/classTestCases.expected.js';
export * from './title/classTitle.expected.js';