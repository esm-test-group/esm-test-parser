import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ClassTestCasesExpectedProps } from './classTestCases.expected.js';
import { ClassTestCasesFixture } from './classTestCases.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ClassTestCases = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ClassTestCasesFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassTestCasesFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassTestCasesExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

}