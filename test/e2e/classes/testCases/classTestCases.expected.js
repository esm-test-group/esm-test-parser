import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassTestCasesExpectedProps = createExpectedProps(
  [
    { name: 'ClassTestCasesFixture', type: TestType.CLASS_DEF },
    { name: 'ClassTestCasesFixture -> test1 (case 1)', type: TestType.CLASS_INST_FUNCTION_WITH_CASES },
    { name: 'ClassTestCasesFixture -> test1 (case 2)', type: TestType.CLASS_INST_FUNCTION_WITH_CASES },
  ]
);
