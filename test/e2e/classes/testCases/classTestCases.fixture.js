import { testCase } from '../../../../src/decorators/all.js';

export class ClassTestCasesFixture {

  @testCase(1, 2, 3)
  @testCase(4, 5, 6)
  ['ClassTestCasesFixture -> test1 (case $i)'](...boundArgs) {
    return {
      thisArg: this,
      boundArgs
    }
  }

}