export * from './basic/classBasic.tests.js';
export * from './builtIn/classBuiltIn.tests.js';
export * from './defaultExport/classDefaultExport.tests.js';
export * from './extends/classExtends.tests.js';
export * from './instance/classInstance.tests.js';
export * from './only/classOnly.tests.js';
export * from './testCases/classTestCases.tests.js';
export * from './title/classTitle.tests.js';