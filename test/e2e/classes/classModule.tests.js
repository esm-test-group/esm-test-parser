import { extractTestsFromModule } from '../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  getGroupMap
} from '../utils.js';
import * as ClassModuleExpectedProps from './all-classes.expected.js';
import * as ClassTestModules from './all-classes.fixtures.js';

let testGroupMap = {};
let testOptions = {};

export const ClassesFixtures = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }

  },

  'Extracts all tests from class module fixture': () => {
    const expectedProps = [];

    Object.keys(ClassModuleExpectedProps)
      .forEach(
        k => expectedProps.push.apply(
          expectedProps,
          ClassModuleExpectedProps[k]
        )
      );

    const actualProps = extractTestsFromModule(ClassTestModules, testOptions)

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    // assert name, type and ordering match expected
    assertProperties(actualProps, expectedProps)

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  },

  "Extracts group names from class module fixture": () => {
    testOptions.sort = "groupsOnly";

    const expectedNames = [
      "root_ClassBasicFixture",
      "root_ClassBuiltInFixture",
      "root_ClassDefaultExportFixture",
      "root_ClassExtendsFixture",
      "root_ClassInstanceFixture",
      "root_ClassOnlyFixture",
      "root_ClassOnlyNumFixture",
      "root_ClassOnlyStarFixture",
      "root_ClassOnlyWithCasesFixture",
      "root_ClassTestCasesFixture",
      "root_ClassTitleFixture -> Title",
      "root_ClassTitleWithBuiltInFixture -> Title"
    ]

    const testProperties = extractTestsFromModule(
      ClassTestModules,
      testOptions
    )

    assertGroupNames(testProperties, expectedNames);
  }

}