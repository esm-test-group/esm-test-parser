import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassDefaultExportExpectedProps = createExpectedProps(
  [
    { name: 'ClassDefaultExportFixture', type: TestType.MODULE },
    { name: 'ClassDefaultExportFixture -> test 1', type: TestType.CLASS_INST_FUNCTION },
  ]
);