import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ClassDefaultExportFixture } from '../all-classes.fixtures.js';
import { ClassDefaultExportExpectedProps } from './classDefaultExport.expected.js';

let testGroupMap = {};
let testOptions = {};

export const ClassDefaultExport = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ClassDefaultExportFixture': () => {
    const actualProps = extractTestsFromModule(
      { ClassDefaultExportFixture: createModule(ClassDefaultExportFixture) },
      testOptions
    )

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    assertProperties(actualProps, ClassDefaultExportExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}