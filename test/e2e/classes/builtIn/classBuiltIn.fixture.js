export class ClassBuiltInFixture {

  beforeAll() {
    return {
      thisArg: this
    }
  }

  beforeEach() {
    return {
      thisArg: this
    }
  }

  afterAll() {
    return {
      thisArg: this
    }
  }

  afterEach() {
    return {
      thisArg: this
    }
  }

  ['ClassBuiltInFixture -> test 1']() {
    return {
      thisArg: this
    }
  }

  ['ClassBuiltInFixture -> test 2']() {
    return {
      thisArg: this
    }
  }

}