import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  createModule,
  getGroupMap
} from '../../utils.js';
import { ClassBuiltInExpectedProps } from './classBuiltIn.expected.js';
import { ClassBuiltInFixture } from './classBuiltIn.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ClassBuiltIn = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }

  },

  'Extracts tests from ClassBuiltInFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassBuiltInFixture }),
      testOptions
    )

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps)

    assertProperties(actualProps, ClassBuiltInExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

}