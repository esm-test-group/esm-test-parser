import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

export const ClassBuiltInExpectedProps = createExpectedProps(
  [
    { name: 'ClassBuiltInFixture', type: TestType.CLASS_DEF },
    { name: 'beforeAll', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
    { name: 'beforeEach', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
    { name: 'afterAll', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
    { name: 'afterEach', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
    { name: 'ClassBuiltInFixture -> test 1', type: TestType.CLASS_INST_FUNCTION },
    { name: 'ClassBuiltInFixture -> test 2', type: TestType.CLASS_INST_FUNCTION },
  ]
);
