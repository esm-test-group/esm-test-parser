export * from './basic/classBasic.fixture.js';
export * from './builtIn/classBuiltIn.fixture.js';
export * as ClassDefaultExportFixture from './defaultExport/classDefaultExport.fixture.js';
export * from './extends/classExtends.fixture.js';
export * from './instance/classInstance.fixture.js';
export * from './only/classOnly.fixture.js';
export * from './testCases/classTestCases.fixture.js';
export * from './title/classTitle.fixture.js';