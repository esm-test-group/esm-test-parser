import { testOnly, testCase } from '../../../../src/decorators/all.js';

@testOnly()
export class ClassOnlyFixture {

  @testOnly()
  ['ClassOnlyFixture -> test 1']() {
    return {
      thisArg: this
    }
  }

  ['ClassOnlyFixture -> test 2']() {
    return {
      thisArg: this
    }
  }

  @testOnly()
  @testCase(1)
  ['ClassOnlyFixture -> only with cases (case $i)'](...boundArgs) {
    return {
      thisArg: this,
      boundArgs
    }
  }

}

export class ClassOnlyStarFixture {

  beforeAll() {
    return {
      thisArg: this
    }
  }

  @testOnly('*')
  ['ClassOnlyStarFixture -> test 1']() {
    return {
      thisArg: this
    }
  }

  ['ClassOnlyStarFixture -> test 2']() {
    return {
      thisArg: this
    }
  }

  @testCase(1)
  ['ClassOnlyStarFixture -> only with cases (case $i)'](...boundArgs) {
    return {
      thisArg: this,
      boundArgs
    }
  }

  afterAll() {
    return {
      thisArg: this
    }
  }

}

export class ClassOnlyNumFixture {

  @testOnly('2')
  ['ClassOnlyNumFixture -> test 1']() {
    return {
      thisArg: this
    }
  }

  ['ClassOnlyNumFixture -> test 2']() {
    return {
      thisArg: this
    }
  }

  @testCase(1)
  ['ClassOnlyNumFixture -> only with cases (case $i)'](...boundArgs) {
    return {
      thisArg: this,
      boundArgs
    }
  }

}

export class ClassOnlyWithCasesFixture {

  @testOnly()
  @testCase(1, 2, 3)
  @testCase(4, 5, 6)
  ['ClassOnlyWithCasesFixture -> test 1 (case $i)'](...boundArgs) {
    return {
      thisArg: this,
      boundArgs
    }
  }

}
