import { TestType } from '../../../../src/parser/models/testType.js';
import { createExpectedProps } from '../../utils.js';

// simulates @testOnly() properties
export const ClassOnlyExpectedProps = createExpectedProps(
  [
    { name: 'ClassOnlyFixture', type: TestType.CLASS_DEF_WITH_ONLY },
    { name: 'ClassOnlyFixture -> test 1', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY },
    { name: 'ClassOnlyFixture -> test 2', type: TestType.CLASS_INST_FUNCTION },
    { name: 'ClassOnlyFixture -> only with cases (case 1)', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY_CASES },
  ]
);

// simulates @testOnly('*') properties
export const ClassOnlyStarExpectedProps = createExpectedProps(
  [
    { name: 'ClassOnlyStarFixture', type: TestType.CLASS_DEF },
    { name: 'beforeAll', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
    { name: 'ClassOnlyStarFixture -> test 1', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY },
    { name: 'ClassOnlyStarFixture -> test 2', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY },
    { name: 'ClassOnlyStarFixture -> only with cases (case 1)', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY_CASES },
    { name: 'afterAll', type: TestType.CLASS_INST_BUILTIN_FUNCTION },
  ]
);

// simulates @testOnly(2) properties
export const ClassOnlyNumExpectedProps = createExpectedProps(
  [
    { name: 'ClassOnlyNumFixture', type: TestType.CLASS_DEF },
    { name: 'ClassOnlyNumFixture -> test 1', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY },
    { name: 'ClassOnlyNumFixture -> test 2', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY },
    { name: 'ClassOnlyNumFixture -> only with cases (case 1)', type: TestType.CLASS_INST_FUNCTION_WITH_CASES },
  ]
);

// simulates @testOnly() on function with test cases properties
export const ClassOnlyWithCasesExpectedProps = createExpectedProps(
  [
    { name: 'ClassOnlyWithCasesFixture', type: TestType.CLASS_DEF },
    { name: 'ClassOnlyWithCasesFixture -> test 1 (case 1)', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY_CASES },
    { name: 'ClassOnlyWithCasesFixture -> test 1 (case 2)', type: TestType.CLASS_INST_FUNCTION_WITH_ONLY_CASES },
  ]
);