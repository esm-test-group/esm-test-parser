import { extractTestsFromModule } from '../../../../src/extractTestsFromModule.js';
import {
  assertFunctions,
  assertGroupNames,
  assertProperties,
  assertTestCaseFunctions,
  createModule,
  getGroupMap
} from '../../utils.js';
import {
  ClassOnlyExpectedProps,
  ClassOnlyNumExpectedProps,
  ClassOnlyStarExpectedProps,
  ClassOnlyWithCasesExpectedProps
} from './classOnly.expected.js';
import {
  ClassOnlyFixture,
  ClassOnlyNumFixture,
  ClassOnlyStarFixture,
  ClassOnlyWithCasesFixture
} from './classOnly.fixture.js';

let testGroupMap = {};
let testOptions = {};

export const ClassOnly = {

  beforeEach: () => {
    testGroupMap = {};
    testOptions = {
      classContextPropertyName: "suiteContext",
      testContextLookup: (groupName) => {
        return testGroupMap[groupName]
      }
    }
  },

  'Extracts tests from ClassOnlyNumFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassOnlyNumFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassOnlyNumExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ClassOnlyStarFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassOnlyStarFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassOnlyStarExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);
  },

  'Extracts tests from ClassOnlyFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassOnlyFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(actualProps, ClassOnlyExpectedProps);

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test functions
    assertFunctions(actualProps, testOptions);

    // assert test functions
    assertTestCaseFunctions(actualProps, testOptions);
  },

  'Extracts tests from ClassOnlyWithCasesFixture': () => {
    const actualProps = extractTestsFromModule(
      createModule({ ClassOnlyWithCasesFixture }),
      testOptions
    );

    // populate the suite map for context lookup
    testGroupMap = getGroupMap(actualProps);

    assertProperties(
      actualProps,
      ClassOnlyWithCasesExpectedProps,
      testOptions
    );

    // assert group names exist in the group map
    assertGroupNames(actualProps, Object.keys(testGroupMap));

    // assert test case functions
    assertTestCaseFunctions(actualProps);
  }

}