# 1.0.0-beta.14

  - Ensured the callback arg from node test is passed into the parsed test

# 1.0.0-beta.13

  - Updated test case title template function to replace `$` arguments.

    - `$` arguments are 1 based. i.e. `$1` will be replaced by the first case argument.
    - `$i` will be replaced by the case index which is also 1 based.

    **note**: `(case #)` is no longer automatically appended to the title.

    Example
    ```js
    'expected $1 to be $2 (case $i)': [
      [[5, 10]],
      function (actual, expected) {
        assert.equal(this.test.title, "expected 5 to be 10 (case 1)");
      }
    ],
    ```

    The template is still customizable using `options.testCaseTitleTemplate`

    ```ts
    // now includes a test case array argument
    (title: string, caseArgs: Array<any[]>, caseIndex: number) => string
    ```

# 1.0.0-beta.12

  - Added `sort` field to `TestParserOptions`.

    Can be either:
    - `"all"`
    - `"groupsOnly"`
    - `"testsOnly"`; or
    - `"none"` (default)

# 1.0.0-beta.11

  - Added ability to have single test cases without writing array brackets for object tests
    ```js
    const fooTests = {
      test1: [
        1,
        2,
        (singleTestCaseArg) => {
          assert.ok(
            singleTestCaseArg == 1
              || singleTestCaseArg == 2
          )
        }
      ]
    }
    ```
  - Renamed AppOptions to TestParserOptions
  - Renamed ParserFlags to TestParserFlags

# 1.0.0-beta.10

  - Breaking change: @only is now @testOnly
    ```js
    @testOnly()
    export class FooTests {}
    ```
  - added parserFlags to AppOptions
    - `parserFlags.allowLiteralOnly` (default is true)
    - `parserFlags.allowLiteralTitle` (default is true)
    - `parserFlags.allowLiteralCases` (default is true)
  - Added exports for 

    `only`, `test.cases`, `test.only` and `test.title`

    Object tests
    ```js
      export const FooTests = {
        [only]: 1, // or [test.only]: 1
        [test.title]: 'test title',

        // these still work and can be disabled using parserFlags
        title: 'still works'
        only: 1 // still works
      }
    ```

    Function tests
    ```js
      fooTest1[only] = true; // or fooTest1[test.only] = true
      fooTest1[test.title] = true;
      export function fooTest1() { }

      fooTest2[test.cases] = [ // or fooTest2["testCases"]
        [Math.PI, 3],
        [Math.PI * 2, 6]
      ]
      export function fooTest2(testRadians, expectedFloor) {
        const actual = Math.floor(testRadians);
        assert.equal(actual, expectedFloor);
      }
    ```
  - added ability to change testCase titles using a function to provide a template
    ```js
    const options = {
      testCaseTitleTemplate: (title, caseIndex) => 
        `${title} (case ${caseIndex})` // the current default
    }
    ```

# 1.0.0-beta.9

- Added ability to export a title variable as the module title (not for root suite yet)
- suiteName is now groupName in TestProperty
- Improved Field and TestProperty processing semantics. It's now pre and post processing.
- added test coverage reports to gitlab ci jobs

# 1.0.0-beta.8
- Added ability to give object test groups a title
  ```js
  export const myTests = {
    title: "Test group title",
    test1: () => assert.ok(true)
  }
  ```

# 1.0.0-beta.7
- Added 'pure function tests' feature for simple tests
  and quick proto typing
- Fixed a bug where `testCases` and `only` combined was not working

# 1.0.0-beta.6

- Fixed an `only:'*'` grouping bug where sibiling groups were not marked to run as only
- Added initial documentation examples (work in progress)

# 1.0.0-beta.5

- Added only expressions. '*', '#num'. 
  
  Examples
  - `only: '*'` will run all the following tests. Decorator syntax `@only("*")`
  - `only: '2'` will run the next two tests. Decorator syntax `@only(2)`
  - `only: true` still runs the one test. Decorator syntax `@only()`

- Refined the recurseModuleProperties in to separate processors

# 1.0.0-beta.4

 - Added `extractTestsFromModule` to extract tests into a simpler flat array
 - Added `EsmPropertyType` class to simplify inspecting test property types
 - Added `EsmProperty class`
 - Added `testContextLookup` to `EsmTestOptions` 
   so a plugin can pass in test contexts to test functions and class instances
 - `extractTestsFromModule` and `EsmPropertyType` are now the main exports for this library
 - Added end-to-end tests for objects and classes

# 1.0.0-beta.3

 - Added cjs support

# 1.0.0-beta.2

 - Fixed testCase decorator execution order
 - Removed static class parsing

# 1.0.0-beta.1

 - Extracted parser code from mocha-ui-esm