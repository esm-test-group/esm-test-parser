import fs from 'node:fs';
import path from 'node:path';
import yaml from 'js-yaml';

const projectPath = process.cwd();
const buildPath = path.resolve(projectPath, "build");
const legacyPath = path.resolve(projectPath, "legacy");
const docsPath = path.resolve(projectPath, "docs");
const examplesPath = path.resolve(projectPath, "test", "examples");

export const build = {
  buildPath,
  projectPath,
  legacyPath,
  docsPath,
  examplesPath
};

export const resolve = path.resolve;
export const relative = path.relative;

export const readFileSync = fs.readFileSync;

export function recurseDirectories(absolutePath) {
  const results = []
  fs.readdirSync(absolutePath)
    .map(dirName => path.resolve(absolutePath, dirName))
    .filter(testPath => fs.statSync(testPath).isDirectory())
    .forEach(dirPath => {
      results.push(dirPath);
      const childDirs = recurseDirectories(dirPath);
      results.push.apply(results, childDirs);
    })

  return results;
}

export function getFiles(absolutePath) {
  return fs.readdirSync(absolutePath)
    .filter(entry => fs.statSync(path.resolve(absolutePath, entry)).isFile())
}

export function replaceFirstInFile(absFilePath, original, replace) {
  const fileContents = fs.readFileSync(absFilePath, 'utf8');
  fs.writeFileSync(absFilePath, fileContents.replace(original, replace));
}

export function replaceAllInFile(absFilePath, original, replace) {
  const fileContents = fs.readFileSync(absFilePath, 'utf8');
  fs.writeFileSync(absFilePath, fileContents.replaceAll(original, replace));
}