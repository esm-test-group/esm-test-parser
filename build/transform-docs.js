import fs from 'node:fs';
import { 
  build, 
  recurseDirectories, 
  getFiles,
  resolve,
  relative
} from './utils.js';

const backTickType = process.argv[2] || "js";
const hiddenCommentTag = `[//${backTickType}]`;
const endHiddenCommentTag = "[/]";
const backTickTag = "```" + backTickType;
const endBackTickTag = "```"
const eol = "\n";
const replaceEol = "\r\n";

const docsChildDirs = [
  '.',
  ...recurseDirectories(build.docsPath)
];

let totalChanges = 0;

docsChildDirs.forEach(docDir => {
  const docDirPath = resolve(build.docsPath, docDir);
  const mdFilePaths = getFiles(docDirPath)
    .map(file => resolve(docDirPath, file));

  mdFilePaths
    // .filter(x => x.includes("grouping-with-exports"))
    .forEach(mdFilePath => {

      // read md file content
      let mdfileContents = fs.readFileSync(mdFilePath, 'utf8');
      mdfileContents = mdfileContents.replaceAll(replaceEol, eol)

      let changes = 0;
      let hiddenCommentCursor = 0;
      while (true) {

        // find the next hidden comment
        const hiddenComment = findNextText(
          mdfileContents,
          hiddenCommentTag,
          endHiddenCommentTag,
          hiddenCommentCursor
        );

        // check if we have a hidden comment
        if (hiddenComment === undefined) break;

        // check snippet file exists
        if (fs.existsSync(hiddenComment.innerContent) === false) {
          throw new Error(
            `Could not find ${hiddenComment.innerContent} snippet file.\n In ${mdFilePath}`
          );
        }

        const snippetRelPath = relative(build.projectPath, hiddenComment.innerContent);
        const snippetRelLink = `[${snippetRelPath}]()\n\n`;

        // read snippet file
        let snippetContent = fs.readFileSync(hiddenComment.innerContent, "utf8");
        snippetContent = snippetContent.replaceAll(replaceEol, eol);

        const newBackTickContent = `${backTickTag}${eol}${snippetContent}${eol}${endBackTickTag}`;

        // try to get the proceeding back tick
        const nextBackTick = findNextText(
          mdfileContents,
          backTickTag,
          endBackTickTag,
          hiddenComment.end
        );

        // if no back tick insert the snippet after the hidden comment
        if (nextBackTick === undefined) {
          mdfileContents = mdfileContents.substring(0, hiddenComment.end + 1)
            + newBackTickContent
            + mdfileContents.substring(hiddenComment.end);

          hiddenCommentCursor = hiddenComment.end + newBackTickContent.length;
          changes++;
          continue;
        }

        // if a back tick replace it's content using the backtick positioning
        if (nextBackTick.innerContent !== snippetContent) {
          mdfileContents = mdfileContents.substring(0, nextBackTick.start)
            + newBackTickContent
            + mdfileContents.substring(nextBackTick.end);

          hiddenCommentCursor = nextBackTick.start + newBackTickContent.length;
          changes++;
          continue;
        }

        // carry on from the last hidden comment
        hiddenCommentCursor = hiddenComment.end;
      }

      // save any md changes
      if (changes > 0) {
        fs.writeFileSync(mdFilePath, mdfileContents, { encoding: "utf8" });
      }

      totalChanges += changes;
    });

});

console.log(`Made ${totalChanges} change(s)`)

function findNextText(content, startText, endText, fromIndex) {
  // position of "start text"
  fromIndex = content.indexOf(startText, fromIndex);
  if (fromIndex === -1) return;

  // position of "end text"
  const afterStartTextIndex = fromIndex + startText.length + 1;
  let end = content.indexOf(
    endText,
    afterStartTextIndex
  );

  // check if we have any end text
  if (end === -1) return;

  const innerContent = content.substring(
    afterStartTextIndex,
    end
  ).trim();

  return {
    innerContent,
    start: fromIndex,
    end: end + endText.length
  };
}