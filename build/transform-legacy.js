import fs from 'node:fs';
import path from 'node:path';
import {
  build,
  recurseDirectories,
  getFiles,
  replaceAllInFile,
  replaceFirstInFile
} from './utils.js';

const jsDirectories = [
  '.',
  ...recurseDirectories(build.legacyPath)
];

jsDirectories.forEach(legacyDir => {
  const legacyDirPath = path.resolve(build.legacyPath, legacyDir);
  const jsFiles = getFiles(legacyDirPath);
  replaceJsFiles(legacyDirPath, jsFiles);
});

function replaceJsFiles(legacyPath, jsFiles) {

  jsFiles.filter(file => path.extname(file) == '.js')
    .forEach(file => {
      const filename = path.parse(file).name;

      // replace all '.js' with '.cjs'
      const jsFilePath = path.resolve(legacyPath, `${filename}.js`);
      replaceAllInFile(jsFilePath, '.js', '.cjs')

      // replace the first map entry from '.js' to '.cjs'
      const mapFilePath = path.resolve(legacyPath, `${filename}.js.map`);
      replaceFirstInFile(mapFilePath, '.js', '.cjs')

      // rename *.js to *.cjs
      fs.renameSync(
        jsFilePath,
        path.resolve(legacyPath, `${filename}.cjs`)
      );

      // rename *.js.map to *.cjs.map
      fs.renameSync(
        mapFilePath,
        path.resolve(legacyPath, `${filename}.cjs.map`)
      );
    });

}